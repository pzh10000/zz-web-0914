import { specslist } from '../../../request/api';
export default {
  state: {
    specsList: [],
  },
  getters: {
    get_specsList(state) {
      return state.specsList;
    },
  },
  mutations: {
    mutation_specsList(state, payload) {
      state.specsList = payload;
    },
  },
  actions: {
    action_specsList({ commit }) {
      specslist()
        .then((res) => {
          // console.log(res);
          if (res.code == 200) {
            commit('mutation_specsList', res.list);
          }
        })
        .catch((err) => {
          console.log(err);
        });
    },
  },
  namespaced: true,
};
