import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';

// 引入全局的css和js
import './assets/css/base.css';
import './assets/js/rem';

// 全局引入vant
import Vant from 'vant';
import 'vant/lib/index.css';
Vue.use(Vant);

// 引入全局组件
import './common'

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
