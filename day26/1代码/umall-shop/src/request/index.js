import axios from 'axios';
import qs from 'querystring';
const baseUrl = '/api'; // 基础url地址

import router from '../router';
import { Toast } from 'vant';

// 请求拦截
axios.interceptors.request.use((req) => {
  // 任何请求，都带上token
  let userInfo = sessionStorage.getItem('userInfo');
  let token = userInfo ? JSON.parse(userInfo).token : '';
  req.headers.authorization = token;
  return req;
});

// 响应拦截
axios.interceptors.response.use((res) => {
  // 全局错误拦截
  if (
    res.data.msg == '请设置请求头，并携带验证字符串' ||
    res.data.msg == '登录已过期或访问权限受限'
  ) {
    // 提示 跳转
    Toast(res.data.msg.toString());
    router.push('/login');
    return;
  }
  return res.data;
});

/* 
  二次封装get方法
  url：指调取的接口地址
  params：数据参数对象
*/
export const get = (url, params = {}) => {
  return new Promise((resolve, reject) => {
    axios
      .get(baseUrl + url, {
        params,
      })
      .then((res) => {
        resolve(res);
      })
      .catch((err) => {
        reject(err);
      });
  });
};

/* 
  二次封装post方法
  url：指调取的接口地址
  params：数据参数对象
  isFile：是否有上传文件，默认不上传文件
*/
export const post = (url, params = {}, isFile = false) => {
  let data = null; // 数据
  if (isFile) {
    // 上传，循环将数据放入到FormData对象中
    data = new FormData();
    for (let attr in params) {
      data.append(attr, params[attr]);
    }
  } else {
    // 没有文件上传，则把数据拼成查询字符串
    data = qs.stringify(params);
  }

  return new Promise((resolve, reject) => {
    axios
      .post(baseUrl + url, data)
      .then((res) => {
        resolve(res);
      })
      .catch((err) => {
        reject(err);
      });
  });
};
