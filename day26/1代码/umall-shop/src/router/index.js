import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [
  {
    path: '/index',
    component: () => import('../pages/index'),
    meta: {
      title: '首页',
      isBack: true, // 是否有回退按钮
    },
    children: [
      {
        path: '/home',
        component: () => import('../views/home'),
        meta: {
          title: '小U商城',
          isBack: false, // 是否有回退按钮
        },
      },
      {
        path: '/sort',
        component: () => import('../views/sort'),
        meta: {
          title: '分类',
          isBack: false, // 是否有回退按钮
        },
      },
      {
        path: '/cart',
        component: () => import('../views/cart'),
        meta: {
          title: '购物车',
          isBack: false, // 是否有回退按钮
        },
      },
      {
        path: '/user',
        component: () => import('../views/user'),
        meta: {
          title: '我的',
          isBack: false, // 是否有回退按钮
        },
      },
      {
        path: '',
        redirect: '/home',
      },
    ],
  },
  {
    path: '/order',
    component: () => import('../pages/order'),
    meta: {
      title: '订单',
      isBack: true, // 是否有回退按钮
    },
  },
  {
    path: '/search',
    component: () => import('../pages/search'),
    meta: {
      title: '搜索',
      isBack: true, // 是否有回退按钮
    },
  },
  {
    path: '/detail',
    component: () => import('../pages/detail'),
    meta: {
      title: '商品详情',
      isBack: true, // 是否有回退按钮
    },
  },
  {
    path: '/list',
    component: () => import('../pages/list'),
    meta: {
      title: '列表',
      isBack: true, // 是否有回退按钮
    },
  },
  {
    path: '/register',
    component: () => import('../pages/register'),
    meta: {
      title: '注册',
      isBack: true, // 是否有回退按钮
    },
  },
  {
    path: '/login',
    component: () => import('../pages/login'),
    meta: {
      title: '登录',
      isBack: true, // 是否有回退按钮
    },
  },
  {
    path: '*',
    redirect: '/index',
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

// 全局路由导航守卫
// 如果需要权限的路由，就放在这个数组中
let arr = ['/cart', '/user'];

router.beforeEach((to, from, next) => {
  // 如果要去的路由在数组里面，就要检查本地存储，如果本地存储没有，就要去登录，如果有，就放行
  // console.log(to.path);
  // console.log(from.path);
  if (arr.includes(to.path)) {
    if (sessionStorage.getItem('userInfo')) {
      next();
    } else {
      next('/login')
    }
  } else {
    next();
  }
});

export default router;
