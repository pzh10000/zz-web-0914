import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

import vHome from '../components/home'
import vList from '../components/list'

const router = new VueRouter({
  routes:[
    {
      path: '/home',
      component: vHome
    },
    {
      path:'/list',
      component: vList
    },
    // 路由重定向，即上面的都匹配不上，到这里来
    {
      path: '*',
      redirect: '/home'
    }
  ]
})

export default router
