import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

// 引入一级组件
import vIndex from '../pages/index';
import vList from '../pages/list';
import vDetail from '../pages/detail';

// 引入二级组件
import rg from '../views/rg';
import tj from '../views/tj';
import search from '../views/search';

const router = new VueRouter({
  routes: [
    {
      path: '/index',
      component: vIndex,

      // 在哪里渲染二级，就要在哪个路线下设置children，它也是一个数组，同一级

      // 如果path没有以'/'开头，则路由地址是：/index/tj
      // children: [
      //   {
      //     path: 'tj',
      //     component: tj,
      //   },
      //   {
      //     path: 'rg',
      //     component: rg,
      //   },
      //   {
      //     path: 'search',
      //     component: search,
      //   },
      // ],

      // 如果path地址加是'/'，则路由地址直接就是 '/tj'
      children: [
        {
          path: '/tj',
          component: tj,
        },
        {
          path: '/rg',
          component: rg,
        },
        {
          path: '/search',
          component: search,
        },
        // 二级路由重定向
        {
          path: '',
          redirect: '/tj'
        }
      ],
    },
    {
      path: '/list',
      component: vList,
    },
    {
      path: '/detail',
      component: vDetail,
    },
    // 一级路由重定向
    {
      path: '*',
      redirect: '/index',
    },
  ],
});

export default router;
