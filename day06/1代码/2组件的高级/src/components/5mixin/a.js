export default {
  data() {
    return {
      isShow: false,
    };
  },
  methods: {
    // 打开弹窗
    add() {
      this.isShow = true;
    },
    // 关闭弹窗
    cancel() {
      this.isShow = false;
    },
  },
  mounted () {
    console.log('我挂载完成了');
  }
}