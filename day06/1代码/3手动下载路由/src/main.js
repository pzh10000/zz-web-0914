import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false

import router from './router'; // 引入路由

new Vue({
  render: h => h(App),
  router // 使用路由
}).$mount('#app')
