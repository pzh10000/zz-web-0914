// 引入vue
import Vue from 'vue';

// 引入路由
import VueRouter from 'vue-router';

// 调用
Vue.use(VueRouter);

// console.log(VueRouter); // 构造函数
// console.log(new VueRouter()); // vue-router的实例

// 引入组件
import vHome from '../components/home.vue';
import vList from '../components/list.vue';


// 实例化路由配置对象并导出
export default new VueRouter({
  // 应该写路由配置属性
  routes: [
    // 当你访问/home,展示vHome组件
    {
      path: '/home',
      component: vHome,
    },
    {
      path: '/list',
      component: vList,
    },
  ],
});
