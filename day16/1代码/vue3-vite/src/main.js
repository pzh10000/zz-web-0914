import { createApp } from 'vue';
import App from './App.vue';

let app = createApp(App); // 取得一个app对象，这个对象下面可以挂载全局api

// 注册全局组件
import vCom from './common';
// console.log(vCom);
for (let attr in vCom) {
  app.component(attr, vCom[attr]);
}

// 引入全局方法，挂载到app原型上
import * as tools from './tools'
app.config.globalProperties.$tools = tools;

app.mount('#app');
