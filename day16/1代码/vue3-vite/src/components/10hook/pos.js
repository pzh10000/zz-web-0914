import { ref, reactive, onMounted, onUnmounted } from 'vue';

export default () => {
  let box = ref(null);
  // console.log(box); // 模板中的div节点

  let pos = reactive({ x: 0, y: 0 }); // 图片的座标位置

  function fnMove({ clientX, clientY }) {
    // console.log(clientX,clientY);
    pos.x = clientX - box.value.offsetLeft + 10;
    pos.y = clientY - box.value.offsetTop + 10;
  }

  // vue挂载完成之后，给box绑定滑动事件，调用fnMove
  onMounted(() => {
    box.value.addEventListener('mousemove', fnMove);
  });

  // 当vue实例销毁时取消事件绑定
  onUnmounted(() => {
    box.value.removeEventListener('mousemove', fnMove);
  });

  return {
    box,
    pos,
  };
};
