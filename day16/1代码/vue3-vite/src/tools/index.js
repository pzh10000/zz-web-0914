// 接受价格，返回保留2位小数的字符串
export function toFixed(n) {
  return n.toFixed(2);
}

// 接受时间戳，返回本地格式时间
export function toTime(n) {
  return new Date(n).toLocaleString()
}