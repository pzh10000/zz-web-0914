import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

// 引入一级路由
// import vIndex from '../pages/index';
// import vDetail from '../pages/detail';
// import vList from '../pages/list';
// import vOrder from '../pages/order';
// import vSearch from '../pages/search';

// 路由懒加载（方式一）
// let vIndex = () => import('../pages/index');
// let vDetail = () => import('../pages/detail');
// let vList = () => import('../pages/list');
// let vOrder = () => import('../pages/order');
// let vSearch = () => import('../pages/search');

// 引入二级路由
// import home from '../views/home';
// import cart from '../views/cart';
// import sort from '../views/sort';
// import user from '../views/user';

const routes = [
  // 首页
  {
    path: '/index',
    component: () => import('../pages/index'),
    children: [
      {
        path: '/home',
        component: () => import('../views/home'),
        meta: {
          title: '首页',
        },
      },
      {
        path: '/cart',
        component: () => import('../views/cart'),
        alias: '/gouwuche', // 别名，就是说原来要通过/cart访问这个路由，现在通过/gouwuche也能访问这个路由
        meta: {
          title: '购物车',
        },
      },
      {
        path: '/sort',
        component: () => import('../views/sort'),
        meta: {
          title: '分类',
        },
      },
      {
        path: '/user',
        component: () => import('../views/user'),
        name: '个人中心',
        meta: {
          a: 1,
          b: 2,
          title: '个人中心',
        },
      },
      {
        path: '',
        redirect: '/home',
      },
    ],
  },
  // 详情
  {
    path: '/detail/:id',
    component: () => import('../pages/detail'),
    name: '商品详情',

    // 路由独享守卫
    beforeEnter(to, from, next) {
      // console.log(to.path, '路由独享守卫--到哪去');
      // console.log(from.path, '路由独享守卫--从哪来');
      next();
    },
  },
  // 列表
  {
    path: '/list',
    component: () => import('../pages/list'),
    beforeEnter(to, from, next) {
      // 想进到列表页，必须从分类进，如果不是从分类来的，则让你到首页去
      if (from.path == '/sort') {
        next();
      } else {
        next('/home');
      }
    },
  },
  // 订单
  {
    path: '/order',
    component: () => import('../pages/order'),
  },
  // 搜索
  {
    path: '/search',
    // 一个路由对应一个组件
    // component: () => import('../pages/search'),
    // 一个路由对应多个组件
    components: {
      default: () => import('../pages/search'),
      abc: () => import('../pages/test'),
    },
  },
  {
    path: '/login',
    component: () => import('../pages/login'),
  },
  {
    path: '*',
    redirect: '/index',
  },
];

const router = new VueRouter({
  routes,
  // mode: 'hash' // 默认模式，地址上会有#号
  // mode: 'history', // 地址没有#号

  // 路由的滚动行为
  scrollBehavior(to, from, position) {
    console.log(position, '滚动位置');
    // 如果用户滑动了滚轴，那么position就有数据，我们可以直接返回该数据，否则清空位置
    if (position) {
      return position;
    } else {
      return {x: 0,y: 0}
    }
  }
});

// 全局路由导航守卫-前置，常用
router.beforeEach((to, from, next) => {
  // console.log('=======全局路由导航守卫-前置======');
  // console.log(to.path, '到哪去');
  // console.log(from.path, '从哪来');

  // 1、如果它去的页面是登录页，我们就next
  if (to.path == '/login') {
    next();
    return;
  }

  // 2、如果它有了登录状态，我们就next
  if (sessionStorage.getItem('user')) {
    next();
    return;
  }

  // 3、如果以上都不符合，我们就强制它去登录
  next('/login');
});

// 全局路由导航守卫-后置，较少使用，没有next
router.afterEach((to, from) => {
  // console.log('=======全局路由导航守卫-后置======');
  // console.log(to.path, '到哪去');
  // console.log(from.path, '从哪来');
});

export default router;
