// 这个模块专门用于管理所有的全局组件
import Vue from 'vue';

// 引入全局组件
import goBack from './goBack';
import goSearch from './goSearch';

let obj = {
  goBack,
  goSearch,
};

// 循环创建
for (let attr in obj) {
  Vue.component(attr, obj[attr]);
}
