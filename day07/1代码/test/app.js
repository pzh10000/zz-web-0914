const express = require('express');
const app = express();
app.listen(3000, () => {
  console.log('http://localhost:3000');
})

// 引入中间件
// 解决history在服务器上刷新出错的问题，使用connect-history-api-fallback中间件
var history = require('connect-history-api-fallback');
app.use(history());

const path = require('path');
app.use(express.static(path.join(__dirname, './static'))); // 开启static文件夹静态资源