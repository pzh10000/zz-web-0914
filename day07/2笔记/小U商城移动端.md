## 小U商城移动端

### 一、创建项目

```
vue create umall-shop
```

### 二、项目初始化

```js
一、删除components下面的helloword.vue和assets下面的图片，删除views文件夹
二、初始化路由的模块

import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

const routes = []
const router = new VueRouter({
  routes
})
export default router

三、初始化App.vue

<template>
  <div>
    <h1>主组件</h1>
  </div>
</template>

<script>
export default {
  data() {
    return {};
  },
};
</script>

<style scoped>
</style>
```



### 三、分析产品需求看是否为pc端或者是移动端

**全局引入rem.js并清除默认样式**

在assets下面创建js文件夹和css文件夹，放入重置css文件和rem.js文件

main.js

```js
// 引入清除默认样式
import './assets/css/reset.css'
// 引入rem.js
import './assets/js/rem'
```

### 四、根据分析创建出一二级组件

一级组件放在pages中，二级放在views中，其它组件放在component中

五个一级组件，四个二级组件

```
首页：index.vue
	首页：home.vue
	分类：sort.vue
	购物车：cart.vue
	个人中心：user.vue
订单：order.vue
搜索：search.vue
商品详情：detail.vue
商品列表：list.vue
```

![](img/QQ截图20220212155911.png)

### 五、封装一二级路由

设置好一二级路由重定向

```js
import Vue from 'vue';
import VueRouter from 'vue-router';

// 引入一级路由
import vIndex from '../pages/index';
import vDetail from '../pages/detail';
import vList from '../pages/list';
import vOrder from '../pages/order';
import vSearch from '../pages/search';

// 引入二级路由
import cart from '../views/cart';
import home from '../views/home';
import sort from '../views/sort';
import user from '../views/user';

Vue.use(VueRouter);

const routes = [
  {
    path: '/index',
    component: vIndex,
    children: [
      {
        path: '/home',
        component: home,
      },
      {
        path: '/cart',
        component: cart,
      },
      {
        path: '/sort',
        component: sort,
      },
      {
        path: '/user',
        component: user,
      },
      {
        path: '',
        redirect: '/home',
      },
    ],
  },
  {
    path: '/detail',
    component: vDetail,
  },
  {
    path: '/list',
    component: vList,
  },
  {
    path: '/order',
    component: vOrder,
  },
  {
    path: '/serarch',
    component: vSearch,
  },
  {
    path: '*',
    redirect: '/index',
  },
];

const router = new VueRouter({
  routes,
});

export default router;
```



在app.vue中定义好一级路由出口，在index.vue中定义好二级路由出口

app.vue

```vue
<!-- 一级路由出口 -->
<router-view></router-view>
```

index.vue

```vue
<h1>首页</h1>
<!-- 二级路由出口 -->
<router-view></router-view>
```





### 六、封装底部导航

```vue
<template>
  <div>
    <!-- 二级路由出口 -->
    <router-view></router-view>

    <!-- 底部导航 -->
    <div class="footer">
      <router-link active-class="active" to="/home">home</router-link>
      <router-link active-class="active" to="/sort">分类</router-link>
      <router-link active-class="active" to="/cart">购物车</router-link>
      <router-link active-class="active" to="/user">个人中心</router-link>
    </div>
  </div>
</template>
<script>
export default {
  data() {
    return {};
  },
};
</script>
<style scoped>
.footer {
  position: fixed;
  left: 0;
  bottom: 0;
  width: 100%;
  height: 0.8rem;
  display: flex;
}
.footer a {
  flex: 1;
  text-align: center;
  line-height: 0.8rem;
  color: #333;
  background: #ccc;
  text-decoration: none;
  font-size: 16px;
}
.footer a.active {
  background: yellow;
}
</style>
```





### 七、抽离底部导航

在components=>vNav.vue中

```vue
<template>
    <div class='footer'>
      <router-link activeClass='active' to="/home">商城</router-link>
      <router-link activeClass='active' to="/cate">分类</router-link>
      <router-link activeClass='active' to="/cart">购物车</router-link>
      <router-link activeClass='active' to="/user">个人中心</router-link>
    </div>
</template>

<script>
export default {
    data() {
        return {

        };
    },
};
</script>

<style  scoped>
.footer{
    position: fixed;
    left: 0;
    bottom: 0;
    width: 100%;
    height: 0.8rem;
    line-height: 0.8rem;
    display: flex;
}
.footer a{
    flex:1;
    text-align: center;
    background: #ccc;
}
.active{
    color: green;
    background: goldenrod !important;
}
</style>
```

在index.vue中再引回来

```vue
<template>
  <div>
    <h1>首页</h1>
    <!-- 二级路由出口 -->
    <router-view></router-view>

    <!-- 封装底部导航 -->
    <v-nav></v-nav>
  </div>
</template>
<script>
import vNav from "../components/vNav.vue";
export default {
  data() {
    return {};
  },
  components: {
    vNav,
  },
};
</script>
<style scoped>
</style>
```

到此，移动端基本骨架搭建完成





### 八、封装轮播图组件

参考资料：

https://www.swiper.com.cn/usage/index.html

https://juejin.cn/post/7068200036949032968

```
npm i swiper@5.4.5
```



在component =>vSwiper.vue中创建，再在home.vue中引入

```vue
<template>
  <div class="swiper-container">
    <div class="swiper-wrapper">
      <div class="swiper-slide">
        <img
          src="https://img30.360buyimg.com/pop/s590x470_jfs/t1/68039/21/18020/100942/627dbfe0Ea13812aa/582a6b7ffd8ad9bd.jpg"
          alt=""
        />
      </div>
      <div class="swiper-slide">
        <img
          src="https://img10.360buyimg.com/pop/s590x470_jfs/t1/222393/2/16355/31621/62832cb1Eb6ae05ca/87734275134da414.jpg"
          alt=""
        />
      </div>
      <div class="swiper-slide">
        <img
          src="https://img12.360buyimg.com/babel/s590x470_jfs/t1/9365/1/16981/84159/627dbee4Efab3440d/42f5cabb3723e370.jpg"
          alt=""
        />
      </div>
    </div>
    <!-- 如果需要分页器 -->
    <div class="swiper-pagination"></div>
  </div>
</template>

<script>
import Swiper from "swiper"; // 注意引入的是Swiper
import "swiper/css/swiper.min.css"; // 注意这里的引入

export default {
  data() {
    return {
      bannerList: [], // 轮播图数据，当我们后面请求了数据之后，再动态展示
    };
  },
  mounted() {
    // 实例化轮播
    new Swiper(".swiper-container", {
      loop: true, // 无缝
      autoplay: true, // 自动
      // 小圆点
      pagination: {
        el: ".swiper-pagination",
      },
    });
  },
};
</script>

<style scoped>
.swiper-container {
  width: 100%;
  height: 3rem;
}

.swiper-slide img {
  display: block;
  width: 100%;
  height: 100%;
  object-fit: cover;
}
</style>
```



### 九、搭建首页基本静态骨架

![image-20220520221112781](img/image-20220520221112781.png)

![image-20220520221226596](img/image-20220520221226596.png)



### 十、首页商品列表跳转到商品详情

![image-20220520221440694](img/image-20220520221440694.png)

在home.vue页面

```vue
<template>
  <div>
    <!-- 使用轮播图组件 -->
    <v-swiper></v-swiper>

    <!-- 首页商品切换 -->
    <div class="goods">
      <span>热门推荐</span>
      <span>上新推荐</span>
      <span>所有商品</span>
    </div>

    <!-- 商品列表展示，应该有三块 -->
    <ul class="goodsUl">
      <li v-for="item in goodsList" :key="item.id">
        <img :src="item.img" :alt="item.goodsName" />
        <div>
          <h3>{{ item.goodsName }}</h3>
          <span>现价格：￥{{ item.oldPrice }}</span>
          <del>原价格：￥{{ item.newPrice }}</del>
          <button>立即抢购</button>
        </div>
      </li>
    </ul>
  </div>
</template>
<script>
import vSwiper from "../components/vSwiper.vue";
export default {
  data() {
    return {
      goodsList: [
        {
          id: 1,
          img: "https://img13.360buyimg.com/seckillcms/s140x140_jfs/t1/222039/22/15787/47580/6284a4a8E147848f7/18779175a6e7816c.jpg.webp",
          goodsName: "啄木鸟",
          oldPrice: "234",
          newPrice: "245",
        },
        {
          id: 2,
          img: "https://img14.360buyimg.com/seckillcms/s140x140_jfs/t1/195626/40/5819/203946/60b58249E4d824b8e/3ee77fa965db8ac8.jpg.webp",
          goodsName: "茶具",
          oldPrice: "340",
          newPrice: "234",
        },
        {
          id: 3,
          img: "https://img20.360buyimg.com/seckillcms/s140x140_jfs/t1/26164/16/16181/41294/62848892E2cc41706/d1afca1d357e6e92.jpg.webp",
          goodsName: "串珠",
          oldPrice: "12",
          newPrice: "1200",
        },
        {
          id: 4,
          img: "https://img13.360buyimg.com/seckillcms/s140x140_jfs/t1/59348/7/18088/100190/6285f1a9Eaa0f571e/41e4c86ec2e53998.jpg.webp",
          goodsName: "轻奢",
          oldPrice: "8",
          newPrice: "19",
        },
      ],
    };
  },
  components: {
    vSwiper,
  },
};
</script>
<style scoped>
.goods {
  height: 0.8rem;
  display: flex;
}
.goods span {
  flex: 1;
  text-align: center;
  line-height: 0.8rem;
  font-size: 0.25rem;
}

.goodsUl {
  width: 96%;
  margin: 0 auto;
  margin-bottom: 1rem;
}
.goodsUl li {
  border: 1px solid #ccc;
  padding: 10px;
  display: flex;
  margin-bottom: 0.16rem;
}
.goodsUl li img {
  border: 1px solid #ccc;
  border-radius: 4px;
}
.goodsUl li div {
  flex: 1;
  margin-left: 0.2rem;
}
.goodsUl li div h3 {
  font-size: 0.34rem;
  font-weight: bold;
  margin-bottom: 0.2rem;
}
.goodsUl li div span,
.goodsUl li div del {
  font-size: 0.24rem;
  display: block;
  margin-bottom: 0.16rem;
}
.goodsUl li div span {
  color: red;
}
.goodsUl li div del {
  color: #ccc;
}
</style>
```

**配置好详情的路由，点击li，实现跳转**









在vue中，实现页面之间的跳转，有以下两种方式

* 一、路由导航

  这种方式会多添加一层标签，有可能会影响页面布局

```vue
<ul class="goodsUl">
    <li v-for="item in goodsList" :key="item.id">
        <router-link to="/detail">
            <img :src="item.img" :alt="item.goodsName" />
            <div>
                <h3>{{ item.goodsName }}</h3>
                <span>现价格：￥{{ item.oldPrice }}</span>
                <del>原价格：￥{{ item.newPrice }}</del>
                <button>立即抢购</button>
            </div>
        </router-link>
    </li>
</ul>
```

* 二、编程式导航

  所谓编程式导航，就是编写逻辑，实现跳转

  这里主要用的是$router这个对象原型下面的一些方法，如：push() replace() go() back()
  
  它参照的是原生的 history 的API
  
  https://developer.mozilla.org/zh-CN/docs/Web/API/History
  
  
  
  * 视图
  
  ```vue
  <ul class="goodsUl">
      <li v-for="item in goodsList" :key="item.id" @click="goDetail">
          <img :src="item.img" :alt="item.goodsName" />
          <div>
              <h3>{{ item.goodsName }}</h3>
              <span>现价格：￥{{ item.oldPrice }}</span>
              <del>原价格：￥{{ item.newPrice }}</del>
              <button>立即抢购</button>
          </div>
      </li>
  </ul>
  ```
  
  - 逻辑代码
  
  ```js
  methods: {
      // 点击跳转到商品详情
      goDetail() {
          // console.log(this.$router);
          this.$router.push("/detail");
      },
  },
  ```



可以在详情页面中定义按钮，点击再返回来

```vue
<p class="m20">
    <button @click="$router.go(-1)">go的方式返回</button>
</p>
<p class="m20">
    <button @click="$router.back()">back的方式返回</button>
</p>
```





### 十一、首页商品列表跳转到商品详情携带动态路由参数（动态路由传参）

### 十二、分类组件中的分类列表跳转到商品列表携带query参数(query传参)



### 十三、封装全局组件（main.js中循环创建）

* 在src下创建common文件夹，在里面创建全局的组件（这里创建了两个组件：goBack.vue和goSearch.vue），并同时并创建一个index.js文件，用于管理所有的全局组件

![image-20220511223007878](img/image-20220511223007878.png)

​		src->common->index.js

```js
// 这个模块专门用于管理所有的全局组件
import Vue from 'vue';

// 引入全局组件
import goBack from './goBack';
import goSearch from './goSearch';

let obj = {
  goBack,
  goSearch,
};

// 循环创建
for (let attr in obj) {
  Vue.component(attr, obj[attr]);
}
```

* 在main.js中引入。一定要在实例的前面引入

  main.js中

```js
// 引入全局组件
import './common'
```

- 在需要使用的地方使用全局组件

```vue
<!-- 使用全局的返回组件 -->
<go-back></go-back>
<!-- 全用全局的搜索组件 -->
<go-search></go-search>
```







### 十四、封装全局过滤器

类似于封装全局组件

- 创建filters文件夹，里面放全局过滤器，并在下面创建一个index.js文件，管理全局的过滤器

  ![image-20220511222913861](img/image-20220511222913861.png)

  

  filters->toPrice.js

```js
// 保留num位小数点
export default (price, num=2) => {
    return price.toFixed(num);
}
```

​		filters->toUpper.js

```js
// 转大写
export default (str) => {
    return str.toUpperCase();
}
```

​		filters->index.js

```js
import Vue from 'vue';

// 引入全局过滤器
import toPrice from './toPrice';
import toUpper from './toUpper';

let obj = {
  toPrice,
  toUpper,
};

for (let attr in obj) {
  Vue.filter(attr, obj[attr]);
}
```

- main.js中引入，要在实例化的前面

```js
// 引入全局过滤器
import './filters'
```

- 使用

```html
<p>商品价格：{{ item.price | toPrice(2) }}</p>
```







### 十五、在脚手架中引入iconfont

* 网址

```
https://www.iconfont.cn/
```

1、先下载好字体图标代码，放到assets文件夹下

2、引入到main.js中

```js
// 引入字体图标
import './assets/font/iconfont.css'
```

3、使用

```vue
<button @click="$router.back()">
    <span class="iconfont icon-fanhui"></span>
    返回
</button>
```







### 十六、在脚手架中引入过渡动画

官网：https://animate.style/

1、下载

```sh
npm i animate.css
```

2、引入到main.js中

```js
// 引入过渡动画
import 'animate.css/animate.min.css';
```

3、谁要有过渡动画，就给哪个标签加transition，并用上动画库的类名

- enter-active-class  进来要用的class
- leave-active-class   离开要用的class

```html
<!-- 加上运动效果，animate__slideInDown  滑动向下 -->
<!-- animate__fadeIn 淡入 -->
<transition enter-active-class="animate__animated animate__fadeIn">
    <router-view></router-view>
</transition>
```

