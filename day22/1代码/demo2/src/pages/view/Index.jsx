import React, { Component } from 'react';
import axios from 'axios';
import { getnew } from '../../request/api';

export default class Index extends Component {
  constructor() {
    super();
    this.state = {
      newList: [], // 创建state状态机，它是响应式的
    };
  }

  // 组件挂载完成，请求数据
  componentDidMount() {
    // axios
    //   .get('/api/getnew')
    //   .then((res) => {
    //     console.log(res);
    //     if (res.data.code === 200) {
    //       this.setState({
    //         newList: res.data.list,
    //       });
    //     }
    //   })
    //   .catch((err) => {
    //     console.log(err);
    //   });

    // 用封装好的方法
    getnew({ id: 1, name: 'zs' })
      .then((res) => {
        // console.log(res);
        if (res.code === 200) {
          this.setState({
            newList: res.list,
          });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }
  render() {
    let { newList } = this.state;
    return (
      <div>
        <h1>请求新人专享数据</h1>
        <ul>
          {newList.map((item) => (
            <li key={item.id}>
              <img
                style={{ width: '100px' }}
                src={this.$url + item.img}
                alt=""
              />
              {item.goodsname}
            </li>
          ))}
        </ul>
      </div>
    );
  }
}
