import React, { Component } from 'react';

export default class Wrt extends Component {
  constructor() {
    super();
    this.state = {
      list: [], // 万人团的数据
    };
  }

  // 挂载完成之后，发起fetch请求
  // fetch是原生的方法，赶走接使用
  componentDidMount() {
    // fetch之get请求
    fetch('/api/getgroupon')
      .then((res) => {
        console.log(res);
        return res.json(); // 返回的数据，是下一个then中的res
      })
      .then((res) => {
        console.log(res); // 真正的数据
        if (res.code === 200) {
          this.setState({ list: res.list });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }

  render() {
    let { list } = this.state;
    return (
      <div>
        <h1>fetch之get请求-万人团</h1>
        <ul>
          {list.map((item) => (
            <li key={item.id}>
              <img
                style={{ width: '100px' }}
                src={this.$url + item.img}
                alt=""
              />
              {item.goodsname}
            </li>
          ))}
        </ul>
      </div>
    );
  }
}
