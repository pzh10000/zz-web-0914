import React from 'react';

import { Button } from 'antd-mobile'; // 引入按钮
import { AudioOutline, AudioMutedOutline } from 'antd-mobile-icons'; // 引图标

export default function Index() {
  return (
    <div>
      <h1>antd-ui</h1>

      <h3>按钮</h3>
      <div>
        <Button>default</Button>
        <Button color="primary">Primary</Button>
        <Button color="success">Success</Button>
        <Button color="danger">Danger</Button>
        <Button color="warning">Warning</Button>
      </div>

      <h3>图标</h3>
      <div>
        <AudioOutline fontSize={80} color="#76c6b8" />
        <AudioMutedOutline fontSize={80} color="red" />
      </div>
    </div>
  );
}
