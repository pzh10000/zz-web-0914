import React, { useState } from 'react';
import { login as loginFn } from '../../request/api';

export default function Login() {
  let [user, setUser] = useState({
    phone: '',
    password: '',
  });

  // 登录
  function login() {
    // console.log(user);
    // fetch('/api/login', {
    //   method: 'post',
    //   body: JSON.stringify(user),
    //   headers: {
    //     'Content-Type': 'application/json',
    //   },
    // })
    //   .then((res) => res.json())
    //   .then((res) => {
    //     console.log(res);
    //     if (res.code === 200) {
    //       alert(res.msg);
    //     } else {
    //       alert(res.msg);
    //     }
    //   })
    //   .catch((err) => {
    //     console.log(err);
    //   });

    loginFn(user)
      .then((res) => {
        console.log(res);
        if (res.code === 200) {
          alert(res.msg);
        } else {
          alert(res.msg);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }
  return (
    <div>
      <h1>登录</h1>

      <p>
        手机号：
        <input
          type="text"
          value={user.phone}
          onChange={(e) => setUser({ ...user, phone: e.target.value })}
        />
      </p>
      <p>
        密码：
        <input
          type="text"
          value={user.password}
          onChange={(e) => setUser({ ...user, password: e.target.value })}
        />
      </p>
      <p>
        <button onClick={() => login()}>登录</button>
      </p>
    </div>
  );
}
