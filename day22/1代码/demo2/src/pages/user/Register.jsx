import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';

import qs from 'qs'; // 将对象转成url形式的查询字符串

import { register as reg } from '../../request/api';

export default function Register() {
  let navigate = useNavigate(); // 使用编程式导航

  let [user, setUser] = useState({
    phone: '', // 手机号
    nickname: '', // 昵称
    password: '', // 密码
  });

  // 注册
  function register() {
    // console.log(user);
    // fetch('/api/register', {
    //   method: 'post',
    //   // body: qs.stringify(user),
    //   body: JSON.stringify(user),
    //   headers: {
    //     // 'Content-Type': 'application/x-www-form-urlencoded', // qs.stringify(user)
    //     'Content-Type': 'application/json', // JSON.stringify(user),
    //   },
    // })
    //   .then((res) => res.json())
    //   .then((res) => {
    //     console.log(res);
    //     if (res.code === 200) {
    //       alert(res.msg);
    //       navigate('/login');
    //     } else {
    //       alert(res.msg);
    //     }
    //   })
    //   .catch((err) => {
    //     console.log(err);
    //   });

    // 使用封装好的方法
    reg(user)
      .then((res) => {
        if (res.code === 200) {
          alert(res.msg);
          navigate('/login');
        } else {
          alert(res.msg);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }

  return (
    <div>
      <h1>注册</h1>

      <p>
        手机号：
        <input
          type="text"
          value={user.phone}
          onChange={(e) => setUser({ ...user, phone: e.target.value })}
        />
      </p>
      <p>
        昵称：
        <input
          type="text"
          value={user.nickname}
          onChange={(e) => setUser({ ...user, nickname: e.target.value })}
        />
      </p>
      <p>
        密码：
        <input
          type="text"
          value={user.password}
          onChange={(e) => setUser({ ...user, password: e.target.value })}
        />
      </p>
      <p>
        <button onClick={() => register()}>注册</button>
      </p>
    </div>
  );
}
