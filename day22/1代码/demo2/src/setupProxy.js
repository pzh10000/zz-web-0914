const proxy = require('http-proxy-middleware');
module.exports = (app) => {
  app.use(
    '/api',
    proxy.createProxyMiddleware({
      target: 'http://localhost:3000', // 这里写的是后端被代理的地址
    })
  );
};
