import { get, post } from './index';

// 获取商品分类
export let getnew = () => get('/api/getnew');

// 注册
export let register = (data) => post('/api/register', data);
// 登录
export let login = (data) => post('/api/login', data);
