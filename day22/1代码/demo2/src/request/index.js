import qs from 'qs'; // 第三方框架，一定要安装

/***
 * get请求
 * 参数一：url 地址，注意要传的数据，跟在url的后面。这里判断了url中是否有?，如果有，数据直接在后面加，如果没有，数据在?号后面加
 * 参数二：params 数据
 */
export function get(url, params) {
  if (params) {
    if (url.search(/\?/) === -1) {
      url += '?' + qs.stringify(params);
    } else {
      url += '&' + qs.stringify(params);
    }
  }
  return fetch(url).then((res) => res.json());
}

/***
 * post请求
 * 参数一：url 地址
 * 参数二：params 数据
 * 参数三：isFile 是否有上传文件，默认不上传文件
 */
export function post(url, params = {}, isFile = false) {
  let options = null; // 配置项
  let data = null; // 数据

  if (isFile) {
    // 上传，循环将数据放入到FormData对象中
    data = new FormData();
    for (let attr in params) {
      data.append(attr, params[attr]);
    }
    options = {
      method: 'post',
      body: data,
    };
  } else {
    // 没有文件上传，则把数据拼成查询字符串
    options = {
      method: 'post',
      body: qs.stringify(params),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
    };
  }

  return fetch(url, options).then((res) => res.json());
}
