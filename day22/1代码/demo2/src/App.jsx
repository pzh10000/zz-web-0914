import React, { lazy, Suspense } from 'react';
import { Routes, Route, Navigate } from 'react-router-dom';

let AntdUi = lazy(() => import('./pages/ui/Index')); // 测试antd移动端UI
let News = lazy(() => import('./pages/view/Index')); // 请求新人专享接口
let Wrt = lazy(() => import('./pages/view/Wrt')); // 万人团

let Register = lazy(() => import('./pages/user/Register')); // 注册
let Login = lazy(() => import('./pages/user/Login')); // 注册

export default function App() {
  return (
    <Suspense fallback={<h3>加载中...</h3>}>
      <>
        <Routes>
          {/* 演示antd-ui */}
          <Route path="/antdui" element={<AntdUi></AntdUi>}></Route>
          {/* 请求新人专享接口 */}
          <Route path="/news" element={<News></News>}></Route>

          {/* 万人团-展示fetch的get请求 */}
          <Route path="/wrt" element={<Wrt></Wrt>}></Route>

          {/* fetch-注册 */}
          <Route path="/register" element={<Register></Register>}></Route>
          {/* fetch-登录 */}
          <Route path="/login" element={<Login></Login>}></Route>

          {/* 重定向 */}
          <Route path="*" element={<Navigate to="/wrt"></Navigate>}></Route>
        </Routes>
      </>
    </Suspense>
  );
}
