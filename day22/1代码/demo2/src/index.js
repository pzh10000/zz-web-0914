import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

import { BrowserRouter } from 'react-router-dom';

// 在Component的原型上，添加一个自定义的属性，这个属性值就是服务器地址
// 为什么要在Component的原型上加，因为每一个组件都继承自Component
React.Component.prototype.$url = 'http://localhost:3000';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <BrowserRouter>
    <App />
  </BrowserRouter>
);
