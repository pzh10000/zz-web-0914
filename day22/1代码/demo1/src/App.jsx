import React, { lazy, Suspense } from 'react';

import { Routes, Route, Navigate } from 'react-router-dom';
import PrivateRoute from './tools/PrivateRoute';

// 引入公共样式
import './assets/css/base.css'

// 1、异步引入组件
let Index = lazy(() => import('./pages/Index/Index'));
let Login = lazy(() => import('./pages/Login/Login'));
let DyList = lazy(() => import('./pages/DyList/DyList'));
let DyDetail = lazy(() => import('./pages/DyDetail/DyDetail'));
let JdList = lazy(() => import('./pages/JdList/JdList'));
let JdDetail = lazy(() => import('./pages/JdDetail/JdDetail'));

let Home = lazy(() => import('./views/Home/Home'));
let Order = lazy(() => import('./views/Order/Order'));
let User = lazy(() => import('./views/User/User'));

export default function App() {
  return (
    <Suspense fallback={<h3>加载中...</h3>}>
      <>
        {/* 2、定义路由出口 */}
        <Routes>
          {/* 3、定义路由规则 */}
          <Route path="/index" element={<Index />}>
            <Route path="home" element={<Home />}></Route>

            {/* 定单和我的需要权限 */}
            <Route path="order" element={<PrivateRoute><Order /></PrivateRoute>}></Route>
            <Route path="user" element={<PrivateRoute><User /></PrivateRoute>}></Route>
            {/* 二级路由重定向 */}
            <Route path="" element={<Navigate to="/index/home" />}></Route>
          </Route>
          <Route path="/login" element={<Login />}></Route>
          <Route path="/dylist" element={<DyList />}></Route>
          <Route path="/dydetail/:id" element={<DyDetail />}></Route>
          <Route path="/jdlist" element={<JdList />}></Route>
          <Route path="/jddetail" element={<JdDetail />}></Route>

          {/* 一级路由重定向 */}
          <Route path="*" element={<Navigate to="/index" />}></Route>
        </Routes>
      </>
    </Suspense>
  );
}
