import { Navigate } from 'react-router-dom';

export default function Private(props) {
  // props.children 是被<PrivateRoute></PrivateRoute>嵌套的组件
  let isLogin = sessionStorage.getItem('user'); // 这里应该去检查本地存储中是否有值
  return isLogin ? props.children : <Navigate to={'/login'} replace></Navigate>;
}
