import React from 'react';

import { Outlet, NavLink } from 'react-router-dom';
import './index.css'

export default function Index() {
  return (
    <div>
      {/* 二级路由出口 */}
      <Outlet></Outlet>

      <div className="footBar">
        <NavLink to="/index/home">首页</NavLink>
        <NavLink to="/index/order">订单</NavLink>
        <NavLink to="/index/user">我的</NavLink>
      </div>
    </div>
  );
}
