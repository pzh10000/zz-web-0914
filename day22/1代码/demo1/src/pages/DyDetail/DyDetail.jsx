import React from 'react';
import { useParams } from 'react-router-dom';
// 引入返回组件
import Back from '../../components/Back';

export default function DyDetail() {
  let obj = useParams();
  // console.log(obj);
  return (
    <div>
      <Back></Back>
      <h1>电影详情</h1>
      <p>传过来的动态路由参数是：{obj.id}</p>
    </div>
  );
}
