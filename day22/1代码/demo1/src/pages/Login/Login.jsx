import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import Back from '../../components/Back';

// 在函数组件中，没有state状态机，要用useState来实现状态机，它是一个函数，必须在函数组件内部来调用
// 调用时，传入数据，返回一个数组，数组的第一项是state状态机数据，第二项就是修改state状态机的方法
// 修改state状态机时，只要传入一个新的值，就可以覆盖原来的值

export default function Login() {
  // 1、定义数据，user即state数据，setUser即修改user的方法
  let [user, setUser] = useState({ name: '', pass: '' });
  // console.log(user);

  // 2、调用setUser，修改user数据
  function changeUser(e, attr) {
    // console.log(e.target.value);
    // console.log(attr);
    setUser({
      ...user,
      [attr]: e.target.value.trim(),
    });
  }

  // 使用编程式导航
  let navigate = useNavigate();

  // 点击登录按钮
  function login() {
    // console.log(user); // 用户数据

    if (!user.name || !user.pass) {
      alert('用户名和密码不能为空');
      return;
    }

    if (user.name === 'admin' && user.pass === '111111') {
      // 成功：提示 存本地 跳转
      alert('登录成功');
      sessionStorage.setItem('user', JSON.stringify(user));
      navigate('/index');
    } else {
      alert('登录失败，用户为admin，密码为111111');
    }
  }

  return (
    <div>
      <Back></Back>
      <h1>用户登录</h1>

      <p>
        用户名：
        <input
          type="text"
          value={user.name}
          onChange={(e) => changeUser(e, 'name')}
        />
      </p>
      <p>
        密码：
        <input
          type="text"
          value={user.pass}
          onChange={(e) => changeUser(e, 'pass')}
        />
      </p>
      <p>
        <button onClick={() => login()}>登录</button>
      </p>
    </div>
  );
}
