import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';

// 引入返回组件
import Back from '../../components/Back';
import './dylist.css';

let movies = [
  {
    id: 1,
    name: '唐人街探案 3',
    director: ' 陈思诚',
    grade: 8.8,
    image:
      'https://dss2.baidu.com/6ONYsjip0QIZ8tyhnq/it/u=2940973367,2508319274&fm=58&app=83&f=JPEG?w=400&h=533&s=08E46084CC947CDE3E3C94D003008099',
  },
  {
    id: 2,
    name: '心灵奇旅',
    director: ' 彼特道格特',
    grade: 8.9,
    image:
      'https://dss0.baidu.com/6ONWsjip0QIZ8tyhnq/it/u=131147363,4086758047&fm=58&app=83&f=JPEG?w=400&h=533&s=BB32208A060112FB642FD49C030060AB',
  },
  {
    id: 3,
    name: '拆弹专家 2',
    director: ' 邱礼涛',
    grade: 7.8,
    image:
      'https://dss0.baidu.com/6ONWsjip0QIZ8tyhnq/it/u=700794841,3270068296&fm=58&app=83&f=JPEG?w=400&h=533',
  },
];

export default function DyList() {
  let navigate = useNavigate();

  let [dyList] = useState(movies); // useState调用的数据，就是响应式的数据
  // console.log(dyList);

  return (
    <div>
      <Back></Back>
      <h1>电影列表</h1>
      <hr />
      <ul>
        {dyList.map((item) => (
          <li key={item.id} className="list">
            <img style={{ width: '100px' }} src={item.image} alt="" />
            <div>
              <h3>{item.name}</h3>
              <p>{item.director}</p>
              <p>{item.grade}</p>
              <p>
                <button onClick={() => navigate('/dydetail/' + item.id)}>
                  去详情
                </button>
              </p>
            </div>
          </li>
        ))}
      </ul>
    </div>
  );
}
