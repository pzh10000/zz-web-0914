import React from 'react';
import { useNavigate } from 'react-router-dom';

// 返回组件
export default function Back() {
  let navigate = useNavigate();
  return (
    <>
      <button onClick={() => navigate(-1)}>返回</button>
    </>
  );
}
