import React from 'react';
import { useNavigate } from 'react-router-dom';

export default function User() {
  let navigate = useNavigate();

  // 退出，清除本地存储，跳转到首页
  function out() {
    sessionStorage.removeItem('user');
    navigate('/index');
  }
  return (
    <div>
      <h1>我的</h1>
      <p>
        <button onClick={() => out()}>退出</button>
      </p>
    </div>
  );
}
