import React from 'react';
import { Link, NavLink, useNavigate } from 'react-router-dom';
import './home.css';
export default function Home() {
  let navigate = useNavigate(); // 取得编程式导航的函数

  return (
    <div>
      <img
        className="bannerImg"
        src="https://imgcps.jd.com/img-cubic/creative_server_cia_jdcloud/v2/2000367/10038581373356/FocusFullshop/CkNqZnMvdDEvMTA5ODkwLzE0LzMyNTk5LzIzMTQ3LzYzMzRhMTliRWNkNzhhMzhmLzkyMDU0ZmUzMDQ0MjgxMjgucG5nEgkzLXR5XzBfNTQwAjjvi3pCEwoP5b6V6Iqs55S15ZC56aOOEAFCGQoV6LaF5YC857K-5ZOB5aSn6IGa5LyaEAJCEAoM56uL5Y2z5oqi6LStEAZCCgoG5LyY6YCJEAdYrNPO0JSkAg/cr/s/q.jpg"
        alt=""
      />

      <h2>1、Link</h2>
      <div>
        <Link to="/dylist">电影</Link>
        <Link to="/jdlist">酒店</Link>
      </div>

      <h2>2、NavLink</h2>
      <div>
        <NavLink to="/dylist">电影</NavLink>
        <NavLink to="/jdlist">酒店</NavLink>
      </div>

      <h2>3、push</h2>
      <div>
        <button onClick={() => navigate('/dylist')}>电影</button>
        <button onClick={() => navigate('/jdlist')}>酒店</button>
      </div>

      <h2>4、replace</h2>
      <div>
        <button onClick={() => navigate('/dylist', { replace: true })}>
          电影
        </button>
        <button onClick={() => navigate('/jdlist', { replace: true })}>
          酒店
        </button>
      </div>
    </div>
  );
}
