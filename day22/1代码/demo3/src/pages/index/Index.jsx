import React, { Component } from 'react';

import One from './One';
import Two from './Two';

export default class Index extends Component {
  constructor() {
    super();
    this.state = {
      isShow: true,
    };
  }
  render() {
    let { isShow } = this.state;
    return (
      <div>
        <h1>Index</h1>
        <button onClick={() => this.setState({ isShow: !isShow })}>
          切换one组件显示与隐藏
        </button>
        <hr />

        {isShow ? <One></One> : null}

        <Two></Two>
      </div>
    );
  }
}
