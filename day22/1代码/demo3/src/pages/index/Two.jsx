import React, { Component } from 'react';

import store, { actions } from '../../store';

export default class Two extends Component {
  changeAge(age) {
    // // console.log(age);
    // let action = { type: 'changeAge', age };
    // store.dispatch(action); // 执行到仓库中的reducer函数

    // -----------------
    store.dispatch(actions.changeAge(age));
  }

  componentDidMount() {
    store.subscribe(() => {
      this.setState(store.getState());
    });
  }

  render() {
    let { name, age } = store.getState();

    return (
      <div className="alert alert-info">
        <h1>two</h1>
        <hr />
        <p>姓名是：{name}</p>
        <p>年龄是：{age}</p>
        <hr />
        <p>修改仓库中的数据</p>

        <p>
          <button onClick={() => this.changeAge('年龄如花')}>修改age</button>
        </p>
      </div>
    );
  }
}
