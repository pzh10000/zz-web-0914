import React, { Component } from 'react';

// 拿数据
// 1、引入仓库
import store, { actions } from '../../store';
// console.log(store); // 取得仓库，它下面就有dispatch、getState、subscribe

export default class One extends Component {
  // 修改仓库中的name
  changeName(name) {
    // // console.log(name); // 小芳
    // let action = { type: 'changeName', name: name };

    // store.dispatch(action); // 会调用仓库中的reducer函数，数据是最新的，但是视图没有变（因为没有调用setState）
    // --------------------

    store.dispatch(actions.changeName(name));
  }

  // 加载完成之后，执行订阅监听
  componentDidMount() {
    // 只要仓库中的数据发生变化，它就会执行subscribe监听，subscribe调用，会返回取消监听的方法
    // 这里执行了监听，当组件销毁时，要释放监听
    // 在之前的版本，如果不释放监听，这里会报错，但是新版本做了优化，但是我们还是要在销毁时释放监听
    // this.un就是取消监听的方法，为什么要挂载在this上，因为要在componentWillUnmount这个里面使用
    this.un = store.subscribe(() => {
      // console.log('我执行了');
      // store.getState()本来就是数据对象{}，而setState就要一个数据对象
      this.setState(store.getState());
    });
  }

  // 组件将要销毁时执行取消监听
  componentWillUnmount() {
    // console.log('我要销毁了');
    this.un(); // 取消监听
  }

  render() {
    // 2、调用getState方法，获取数据
    let { name, age } = store.getState(); // 取得数据
    // console.log(name, age); // 悟空 18

    return (
      <div className="alert alert-success">
        <h1>one</h1>
        <hr />
        {/* 3、使用数据 */}
        <p>姓名是：{name}</p>
        <p>年龄是：{age}</p>

        <hr />
        <p>修改仓库中的数据</p>
        <p>
          <button onClick={() => this.changeName('小芳')}>修改name</button>
        </p>
      </div>
    );
  }
}
