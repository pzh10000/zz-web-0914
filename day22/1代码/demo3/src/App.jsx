import React from 'react';

import Index from './pages/index/Index';

export default function App() {
  return (
    <div>
      <Index></Index>
    </div>
  );
}
