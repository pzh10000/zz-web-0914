import { createStore } from 'redux'; // 得到创建store的方法，该方法调用，要传入reducer函数，返回store仓库

// 1、声明state，里面就是放各种数据，它是唯一的数据来源
let initState = {
  name: '悟空',
  age: 18,
};

const TYPES = {
  CHANGENAME: 'CHANGENAME',
  CHANGEAGE: 'CHANGEAGE',
};

// 封装actionCreator
export let actions = {
  changeName: (name) => ({ type: TYPES.CHANGENAME, name }),
  changeAge: (age) => ({ type: TYPES.CHANGEAGE, age }),
};

// 2、声明reducer函数，它有两个参数，一个是state，有初始值，一个是action
function reducer(state = initState, action) {
  // console.log(action);
  switch (action.type) {
    // 假如你传的action是{type:'changeName', name: '八戒'}
    case TYPES.CHANGENAME:
      return {
        ...state,
        name: action.name,
      };

    // 假如你传的action是{type:'changeAge', age: 20}
    case TYPES.CHANGEAGE:
      return {
        ...state,
        age: action.age,
      };

    default:
      return state;
  }
}

// 3、得到store容器并导出，createStore方法调用，接收reducer函数做为参数
export default createStore(reducer);
