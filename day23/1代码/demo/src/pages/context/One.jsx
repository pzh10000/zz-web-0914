import React, { createContext, useState } from 'react';

import Two from './Two';

// 1、获取context，并取得Provider组件
export let myContext = createContext(); // 得到context对象
// console.log(myContext); // 它是一个对象，里面有很多的属性
let { Provider } = myContext; // 导出Provider，Provider为数据的提供者

export default function One() {
  // 2、创建数据
  let [user] = useState({ id: 1, name: '小芳' });

  return (
    <div>
      <h1>one</h1>
      <p>提供的数据是：</p>
      <p>姓名：{user.name}</p>
      <p>id：{user.id}</p>
      <hr />

      {/* 3、将数据提供出去 */}
      <Provider value={user}>
        <Two></Two>
      </Provider>
    </div>
  );
}
