// 4、导出useContext。 useContext接收context作为参数，所以下面要导入myContext
// useContext调用，要传入myContext作为参数
import React, { useContext } from 'react';
import { myContext } from './One';

export default function Three() {
  // console.log(useContext(myContext)); // {id: 1, name: '小芳'}
  let { id, name } = useContext(myContext); // 取得数据

  return (
    <div>
      <h1>three</h1>
      <p>接收过来的数据是：</p>
      <p>姓名：{name}</p>
      <p>id：{id}</p>
    </div>
  );
}
