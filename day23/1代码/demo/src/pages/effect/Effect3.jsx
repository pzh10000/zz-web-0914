import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

export default function Effect3() {
  let [time, setTime] = useState(new Date());

  // 第四种使用方式，函数有一个返回函数，这个函数相当于组件将要销毁
  useEffect(() => {
    let timer = setTimeout(() => {
      // console.log(123);
      setTime(new Date());
    }, 1000);

    // return的函数，相当于组件将要销毁（在这个里面就可以清除定时器）
    return () => {
      // console.log('我销毁了');
      clearTimeout(timer);
    };
  });

  return (
    <div>
      <Link to="/effect2">到Effect2</Link>

      <h1>Effect的组件将要销毁</h1>

      <p>当前时间是：{time.toLocaleString()}</p>
    </div>
  );
}
