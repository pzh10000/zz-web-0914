import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom'

export default function Effect2() {
  let [num, setNum] = useState(0); // 函数组件中的状态机
  let [name, setName] = useState('小花');

  // 使用方式一：只有一个回调函数，相当于"挂载完成"和"更新完成"
  // useEffect(() => {
  //   document.title = `点击页面${num}次`;
  // });

  // 使用方式二：有第二个参数，第二个参数为空数组，则函数相当于"挂载完成"
  // useEffect(() => {
  //   document.title = `点击页面${num}次`;
  // }, []);

  // 使用方式三：有第二个参数，第二个参数数组有值，则这个值的变化会触发"挂载完成"和"更新完成"
  useEffect(() => {
    document.title = `点击页面${num}次，姓名是${name}`;
  }, [num]);

  return (
    <div>
      <Link to="/effect3">到Effect3</Link>
      <h1>函数组件演示生命周期</h1>

      <hr />
      <p>点击页面{num}次</p>
      <p>
        <button onClick={() => setNum(++num)}>点击增加一次</button>
      </p>

      <hr />
      <p>姓名是：{name}</p>
      <p>
        <button onClick={() => setName('小芳'+Math.random())}>修改姓名</button>
      </p>
    </div>
  );
}
