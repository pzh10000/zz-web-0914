import React, { Component } from 'react';

export default class Effect1 extends Component {
  constructor() {
    super();
    this.state = {
      num: 0,
    };
  }

  // 挂载完成将次数放在title上
  componentDidMount() {
    let { num } = this.state;
    document.title = `你点击页面${num}次`;
  }

  // 组件更新之后，重新设置title
  componentDidUpdate() {
    let { num } = this.state;
    document.title = `你点击页面${num}次`;
  }

  render() {
    let { num } = this.state; // 解构出点击页面的次数
    return (
      <div>
        <h1>类组件演示生命周期</h1>
        <p>你点击页面{num}次</p>
        <hr />
        
        <p>
          <button onClick={() => this.setState({ num: ++num })}>
            点击页面增加一次
          </button>
        </p>
      </div>
    );
  }
}
