import React, { useState } from 'react';

// useState只能在函数组件中调用，返回一个数组，第一项为传的数据，第二项为修改数据的方法

export default function Index() {
  // 调用useState，相当于产生state状态机
  let [str, setStr] = useState('hello'); // 第一项为数据，第二项为修改数据的方法

  let [user, setUser] = useState({ id: 1, name: '小芳' });

  return (
    <div>
      <h1>useState</h1>

      <hr />
      <p>欢迎你：{str}</p>
      <p>
        <button onClick={() => setStr('你好')}>修改</button>
      </p>

      <hr />
      <p>你的姓名是：{user.name}</p>
      <p>
        <button onClick={() => setUser({ ...user, name: '小花' })}>
          修改姓名
        </button>
      </p>
    </div>
  );
}
