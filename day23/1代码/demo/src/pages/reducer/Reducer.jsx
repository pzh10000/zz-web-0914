import React, { useReducer } from 'react';

// useReducer它接收两个参数
// 参数一:reducer函数
// 参数二:state初始值
// 返回值:返回state的初始值和dispatch

// 1、声明state
let initState = {
  num: 888,
};

// 3、封装actionType
let TYPES = {
  ADD: 'ADD', // 添加
  MINUS: 'MINUS', // 减少
};

// 2、声明reducer，它有两个参数
function reducer(state = initState, action) {
  switch (action.type) {
    case TYPES.ADD:
      return {
        ...state,
        num: action.num,
      };
    case TYPES.MINUS:
      return {
        ...state,
        num: action.num,
      };
    default:
      return state;
  }
}

export default function Reducer() {
  let [state, dispatch] = useReducer(reducer, initState); // 返回数据和dispatch方法

  return (
    <div>
      <h1>useReducer的基本使用</h1>

      <hr />
      <p>数据是：{state.num}</p>
      
      <p>
        <button onClick={() => dispatch({ type: TYPES.ADD, num: ++state.num })}>
          增加数字
        </button>
      </p>
      <p>
        <button
          onClick={() => dispatch({ type: TYPES.MINUS, num: --state.num })}
        >
          减少数字
        </button>
      </p>
    </div>
  );
}
