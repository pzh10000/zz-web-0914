import React, { useReducer, useEffect } from 'react';

// 1、创建state
let initState = {
  news: [],
};

// 2、types
let TYPES = {
  CHANGE_NEWS: 'CHANGE_NEWS',
};

// 3、reducer
function reducer(state = initState, action) {
  switch (action.type) {
    case TYPES.CHANGE_NEWS:
      return {
        ...state,
        news: action.news,
      };
    default:
      return state;
  }
}

export default function Reducer2() {
  // 取得仓库中的数据
  let [obj, dispatch] = useReducer(reducer, initState);
  // console.log(obj);
  // console.log(dispatch);

  // 当挂载完成，发起ajax请求
  useEffect(() => {
    fetch('/api/getnew')
      .then((res) => res.json())
      .then((res) => {
        console.log(res);
        if (res.code === 200) {
          dispatch({ type: TYPES.CHANGE_NEWS, news: res.list });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  return (
    <div>
      <h1>useReducer在ajax请求中的用处</h1>

      <ul>
        {obj.news.map((item) => (
          <li key={item.id}>
            {item.goodsname}
            <img style={{ width: '100px' }} src={item.img} alt="" />
          </li>
        ))}
      </ul>
    </div>
  );
}
