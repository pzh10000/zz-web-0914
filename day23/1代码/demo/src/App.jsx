import React, { lazy, Suspense } from 'react';

import { Routes, Route, Navigate } from 'react-router-dom';

// 引入组件
let StateIndex = lazy(() => import('./pages/state/Index'));
let Effect1 = lazy(() => import('./pages/effect/Effect1'));
let Effect2 = lazy(() => import('./pages/effect/Effect2'));
let Effect3 = lazy(() => import('./pages/effect/Effect3'));
let Reducer = lazy(() => import('./pages/reducer/Reducer'));
let Reducer2 = lazy(() => import('./pages/reducer/Reducer2'));
let Context = lazy(() => import('./pages/context/One'));

export default function App() {
  return (
    <Suspense fallback={<h3>加载中...</h3>}>
      <>
        {/* 路由出口 */}
        <Routes>
          {/* 路由规则 */}
          <Route path="/state" element={<StateIndex></StateIndex>}></Route>
          <Route path="/effect1" element={<Effect1></Effect1>}></Route>
          <Route path="/effect2" element={<Effect2></Effect2>}></Route>
          <Route path="/effect3" element={<Effect3></Effect3>}></Route>
          <Route path="/reducer" element={<Reducer></Reducer>}></Route>
          <Route path="/reducer2" element={<Reducer2></Reducer2>}></Route>
          <Route path="/context" element={<Context></Context>}></Route>

          {/* 路由重定向 */}
          <Route
            path="*"
            element={<Navigate to="/Context"></Navigate>}
          ></Route>
        </Routes>
      </>
    </Suspense>
  );
}
