import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [
  {
    path: '/about',
    component: () => import('../components/about.vue'),
  },
  {
    path: '/menu',
    component: () => import('../components/menu.vue'),
  },
  {
    path: '*',
    redirect: '/about',
  },
];

const router = new VueRouter({
  routes,
});

export default router;
