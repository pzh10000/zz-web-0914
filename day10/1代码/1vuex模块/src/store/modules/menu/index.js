export default {
  state: {
    name: '我是menu',
  },
  getters: {
    get_name(state) {
      return state.name;
    },
  },
  mutations: {},
  actions: {},
  namespaced: true, // 启用模块命名空间
};
