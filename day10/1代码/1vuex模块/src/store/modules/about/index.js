export default {
  state: {
    name: '我是about',
  },
  getters: {
    get_name(state) {
      return state.name;
    },
  },
  mutations: {
    mutation_name(state, payload) {
      state.name = payload;
    },
  },
  actions: {
    action_name({ commit }) {
      setTimeout(() => {
        commit('mutation_name', Math.random());
      }, 1000);
    },
  },
  namespaced: true, // 启用模块命名空间
};
