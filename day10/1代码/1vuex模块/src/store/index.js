import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

// 导入模块
import about from './modules/about';
import menu from './modules/menu';

export default new Vuex.Store({
  state: {},
  getters: {},
  mutations: {},
  actions: {},
  modules: {
    about,
    menu,
  },
});
