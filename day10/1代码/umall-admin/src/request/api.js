import { get, post } from './index';

// ============菜单管理==============
// 菜单添加
export const menuadd = (data) => post('/menuadd', data);
// 菜单列表
export const menulist = () => get('/menulist?istree=true');
// 菜单删除
export const menudelete = (data) => post('/menudelete', data);
// 菜单获取（一条）
export const menuinfo = (data) => get('/menuinfo', data);
// 菜单修改
export const menuedit = (data) => post('/menuedit', data);
