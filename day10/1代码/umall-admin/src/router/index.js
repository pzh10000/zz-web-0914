import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [
  {
    path: '/login',
    component: () => import('../pages/login.vue'),
  },
  {
    path: '/index',
    component: () => import('../pages/index.vue'),
    children: [
      {
        path: '/home',
        component: () => import('../views/home.vue'),
      },
      {
        path: '/menu',
        component: () => import('../views/menu/menu.vue'),
      },
      {
        path: '/role',
        component: () => import('../views/role.vue'),
      },
      {
        path: '/user',
        component: () => import('../views/user.vue'),
      },
      {
        path: '/cate',
        component: () => import('../views/cate.vue'),
      },
      {
        path: '',
        redirect: '/home',
      },
    ],
  },
  {
    path: '*',
    redirect: '/index',
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
