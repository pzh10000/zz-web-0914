import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';

Vue.config.productionTip = false;

// 全局引入elementUI
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ElementUI);

// 引入清除全局样式
import './assets/css/base.css';

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
