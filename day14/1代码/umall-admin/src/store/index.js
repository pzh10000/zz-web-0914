import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

// 引入模块
import menu from './modules/menu';
import role from './modules/role';
import user from './modules/user';
import cate from './modules/cate';
import specs from './modules/specs';
import goods from './modules/goods'

let userInfo = sessionStorage.getItem('userInfo');

export default new Vuex.Store({
  state: {
    userInfo: userInfo ? JSON.parse(userInfo) : {}, // 每次刷新的时候，都先从本地中找
  },
  getters: {},
  mutations: {
    mutation_user(state, payload) {
      // 如果传过来的值是真的，则向本地存储中存一份，并修改state的值
      // 如果是假的，清除本地存储，并设置state中的值为{}
      if (payload) {
        // 获取数据以后，向本地中存一份
        state.userInfo = payload;
        sessionStorage.setItem('userInfo', JSON.stringify(payload));
      } else {
        state.userInfo = {};
        sessionStorage.removeItem('userInfo');
      }
    },
  },
  actions: {},
  modules: {
    menu,
    role,
    user,
    cate,
    specs,
    goods,
  },
});
