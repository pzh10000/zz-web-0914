import { userlist, usercount } from '../../../request/api';

export default {
  state: {
    userList: [], // 管理员列表
    total: 0, // 总条数
    size: 2, // 每页条数
    page: 1, // 当前页
  },
  getters: {
    get_userList(state) {
      return state.userList;
    },
    get_info(state) {
      return {
        total: state.total, // 总条数
        size: state.size, // 每页条数
        page: state.page, // 当前页码
      };
    },
  },
  mutations: {
    mutation_userList(state, payload) {
      state.userList = payload;
    },
    mutation_total(state, payload) {
      state.total = payload;
    },
    // 同步修改页码
    mutation_page(state, payload) {
      state.page = payload;
    },
  },
  actions: {
    // 获取列表
    action_userList({ commit, state, dispatch }) {
      userlist({
        size: state.size,
        page: state.page,
      })
        .then((res) => {
          // console.log(res);
          if (res.code == 200) {
            // console.log(res.list); // 当前页对应的数据
            // 当前不是第一页，且返回的数据是null会出现
            if (!res.list && state.page !== 1) {
              // 页码减1
              commit('mutation_page', state.page - 1);
              // 再发起ajax请求
              dispatch('action_userList');
              return;
            }

            commit('mutation_userList', res.list);
          }
        })
        .catch((err) => {
          console.log(err);
        });
    },

    // 获取总条数
    action_total({ commit }) {
      usercount()
        .then((res) => {
          // console.log(res);
          if (res.code == 200) {
            commit('mutation_total', res.list[0].total);
          }
        })
        .catch((err) => {
          console.log(err);
        });
    },
  },
  namespaced: true,
};
