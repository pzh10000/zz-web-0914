import { catelist } from '../../../request/api';

export default {
  state: {
    cateList: [],
  },
  getters: {
    get_cateList(state) {
      return state.cateList;
    },
  },
  mutations: {
    mutation_cateList(state, payload) {
      state.cateList = payload;
    },
  },
  actions: {
    action_cateList({ commit }) {
      catelist()
        .then((res) => {
          // console.log(res);
          if (res.code == 200) {
            commit('mutation_cateList', res.list);
          }
        })
        .catch((err) => {
          console.log(err);
        });
    },
  },
  namespaced: true,
};
