import { goodslist } from '../../../request/api';

export default {
  state: {
    goodsList: [],
  },
  getters: {
    get_goodsList(state) {
      return state.goodsList;
    },
  },
  mutations: {
    mutation_goodsList(state, payload) {
      state.goodsList = payload;
    },
  },
  actions: {
    action_goodsList({ commit }) {
      goodslist()
        .then((res) => {
          // console.log(res);
          if (res.code == 200) {
            commit('mutation_goodsList', res.list);
          }
        })
        .catch((err) => {
          console.log(err);
        });
    },
  },
  namespaced: true,
};
