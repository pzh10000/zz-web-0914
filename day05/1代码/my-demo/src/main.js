import Vue from 'vue';
import App from './App.vue';

Vue.config.productionTip = false;

// 全局引入bootstrap
import 'bootstrap/dist/css/bootstrap.css';

// eventBus，中央事件处理，在原型上挂载一个vue实例，这个实例下面就有$emit和$on
Vue.prototype.$event = new Vue();

// 全局引入jq
import $ from 'jquery';
Vue.prototype.$ = $;

let vm = new Vue({
  render: (h) => h(App),
}).$mount('#app');

// console.log(vm);
