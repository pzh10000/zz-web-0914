import React from 'react';

// 1、父子通信案例-列表
// import Index from './component/list/Index';

// 2、生命周期流程
// import Life from './component/life/Life';

// 3、受控组件
// import Form from './component/form/Form';

// 4、非受控组件
// import Form1 from './component/form/Form1';

// 5、ref
// import Ref from './component/ref/Index';

// 6、ref在视频播放中的使用
// import Ref from './component/videoRef/Ref'

// 7、在主组件中操作子组件的dom节点
import Index from './component/forward/Index';

export default function App() {
  return (
    <div>
      <Index></Index>
      {/* <Life></Life> */}
      {/* <Form1></Form1> */}
      {/* <Ref></Ref> */}
    </div>
  );
}
