import React, { Component } from 'react';

import Child from './Child';

export default class Index extends Component {
  constructor() {
    super();

    this.state = {
      // 1、创建一个ref，此时，值为 {current: null}
      item: React.createRef(null),
    };
  }
  render() {
    return (
      <div>
        <h1>主组件</h1>

        <hr />

        {/* 2、找到节点，设置ref属性 */}
        <Child ref={this.state.item}></Child>
      </div>
    );
  }

  // 挂载完成，调用子组件的方法
  componentDidMount() {
    console.log(this.state.item);
    // 3、调用子组件的方法
    this.state.item.current.setFocus();
  }
}
