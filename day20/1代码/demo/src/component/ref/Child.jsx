import React, { Component } from 'react';

export default class Child extends Component {
  constructor() {
    super();

    this.state = {
      input: React.createRef(null),
    };
  }
  render() {
    return (
      <div>
        <h1>子组件</h1>

        <p>
          <input
            type="text"
            ref={this.state.input}
            onFocus={(e) => (e.target.style.background = 'red')}
          />
        </p>
      </div>
    );
  }

  // 封装一个方法，让input元素得到焦点
  setFocus() {
    console.log('我执行了');

    // 方式一：通过DOM获取节点
    // let input = document.querySelector('input');
    // input.focus();

    // 方式二：通过ref获取节点
    this.state.input.current.focus();
  }
}
