import React, { Component } from 'react';

export default class Ref extends Component {
  constructor() {
    super();
    this.state = {
      box: React.createRef(null), // video标签
      video: require('../../assets/video/mov_bbb.mp4'),
      play: require('../../assets/img/play.jpg'),
      pause: require('../../assets/img/pause.jpg'),
      isPlay: false, // 是否处于播放状态
    };
  }
  render() {
    let { video, play, pause, isPlay, box } = this.state;
    return (
      <div>
        <h1>ref在视频播放中的使用</h1>

        <hr />
        <video ref={box} src={video}></video>

        <p>
          {isPlay ? (
            <img src={pause} alt="" onClick={() => this.pause()} />
          ) : (
            <img src={play} alt="" onClick={() => this.play()} />
          )}
        </p>
      </div>
    );
  }

  // 播放
  play() {
    this.setState({ isPlay: true });
    this.state.box.current.play();
  }
  pause() {
    this.setState({ isPlay: false });
    this.state.box.current.pause();
  }
}
