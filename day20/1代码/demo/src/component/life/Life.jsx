import React, { Component } from 'react';

import Child from './Child';
export default class Life extends Component {
  constructor() {
    super();

    this.state = {
      age: 3,
      isShow: true, // 控制子组件显示或隐藏，默认显示
    };
  }

  render() {
    return (
      <div className='container'>
        <h1>父组件</h1>
        <p>使用自己的数据：{this.state.age}</p>

        <p>
          <button onClick={() => this.setState({ isShow: !this.state.isShow })}>
            切换子组件的展示和隐藏，销毁子组件或创建子组件
          </button>
        </p>

        <p>
          <button onClick={() => this.setState({ age: 5 })}>
            更新自己的数据，导致子组件更新
          </button>
        </p>

        <hr />

        {this.state.isShow ? <Child {...this.state}></Child> : null}
      </div>
    );
  }
}
