import React, { Component } from 'react';

export default class Child extends Component {
  // 挂载期
  // 作用:在实例化时,做初始化赋值操作/方法的调用
  // 常用
  constructor() {
    super();
    console.log('constructor');
    // console.log(document.getElementById('head')); // null

    this.state = {
      name: '小王',
    };
  }

  // 静态方法：要用关键字static修饰
  // 是否将props中的数据映射至state上？
  // 1、返回null: 不映射
  // 2、返回nextState: 不映射
  // 3、返回nextProps: 映射
  // 4、根据props中的数据，决定是否映射
  static getDerivedStateFromProps(nextProps, nextState) {
    console.log('getDerivedStateFromProps');
    // console.log(document.getElementById('head')); // null
    // console.log(this); // undefined
    // console.log(nextProps); // props
    // console.log(nextState); // state

    // 第四种
    if (nextProps.age == 3 || nextProps.age == 5) {
      nextState.age = nextProps.age;
      return nextState;
    }

    return null;
  }

  // 渲染（常用）
  render() {
    console.log('render');
    // console.log(document.getElementById('head')); // null

    return (
      <div>
        <h1 id="head">子组件</h1>

        <p>{this.state.name}</p>
        <p>{this.state.age}</p>
      </div>
    );
  }

  // 组件挂载完成（常用）
  // 可以发起轮播图、异步请求、计时器、延时器、订阅等等
  componentDidMount() {
    console.log('componentDidMount');
    // console.log(document.getElementById('head')); // null
  }

  // --------------------------------------------------------------------
  // 更新期
  // 1、getDerivedStateFromProps   是否要将props的数据映射至state

  // 2、应该更新组件吗? 返回true和false
  shouldComponentUpdate() {
    console.log('shouldComponentUpdate');
    return true; // 如果return true,则执行:render getSnapsHotBeforeUpdate componentDidUpdate
    // return false; // 如果return false,则不执行:render getSnapsHotBeforeUpdate componentDidUpdate
  }

  // 渲染
  // 3、render（常用）

  // 4、组件更新之前
  getSnapshotBeforeUpdate(nextProps, nextState) {
    console.log('getSnapshotBeforeUpdate');
    console.log(nextProps); // 之前的props
    console.log(nextState); // 最新的state

    return nextState;
  }

  // 5、组件更新完成（常用）
  componentDidUpdate(prevProps, prevState) {
    console.log('componentDidUpdate');
    console.log(prevProps); // 之前的props
    console.log(prevState); // 最新的state
  }

  // ---------------------------------
  // 销毁期
  // 组件将要销毁（常用）
  // 取消轮播图,定时器,延时器,订阅等等
  componentWillUnmount() {
    console.log('componentWillUnmount');
  }
}
