import React, { Component } from 'react';

import Child from './Child';
export default class Index extends Component {
  constructor() {
    super();
    this.state = {
      name: 'zs',
      box: React.createRef(null),
    };
  }

  render() {
    return (
      <div>
        <h1>主组件</h1>
        <button onClick={() => this.fn()}>
          按钮点击，操作子组件中的某个DOM节点
        </button>

        <hr />

        <Child name={this.state.name} ref={this.state.box}></Child>
      </div>
    );
  }

  fn() {
    // console.log(this.state.box);
    // 取得子组件中的某个DOM节点，设置样式
    this.state.box.current.style.width = '100px';
    this.state.box.current.style.height = '100px';
    this.state.box.current.style.background = 'red';
  }
}
