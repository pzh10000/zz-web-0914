import React, { forwardRef } from 'react';

// 父组件要获取子组件的dom节点，子组件必须声明成函数组件，且必须要用forwardRef调用
// forwardRef()接收一个函数做为参数，这个函数有两个参数，分别是props, ref
export default forwardRef((props, ref) => {
  console.log(props); // {name:'zs}
  console.log(ref); // {current:null}

  return (
    <div>
      <h1>子节点</h1>

      <div ref={ref}></div>
    </div>
  );
});
