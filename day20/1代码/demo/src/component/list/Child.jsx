import React, { Component } from 'react';

import './list.css';

export default class Child extends Component {
  render() {
    // console.log(this.props);
    let { goods, delItem } = this.props; // 取得父组件传过来的数据

    return (
      <div className="container alert alert-info">
        <h2>子组件</h2>

        <table className="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th>序号</th>
              <th>名称</th>
              <th>图片</th>
              <th>价格</th>
              <th>件数</th>
              <th>操作</th>
            </tr>
          </thead>

          <tbody>
            {goods.length ? (
              goods.map((item, index) => (
                <tr key={item.id}>
                  <td>{index + 1}</td>
                  <td>{item.name}</td>
                  <td>
                    <img
                      className="imgBox"
                      src={item.imageUrl}
                      alt={item.name}
                    />
                  </td>
                  <td>{item.price}</td>
                  <td>{item.num}</td>
                  <td>
                    <button
                      onClick={() => delItem(index)}
                      className="btn btn-danger btn-sm"
                    >
                      删除
                    </button>
                  </td>
                </tr>
              ))
            ) : (
              <tr>
                <td colSpan={6}>暂无数据</td>
              </tr>
            )}
          </tbody>
        </table>
      </div>
    );
  }
}
