import React, { Component } from 'react';

import Child from './Child';

export default class Index extends Component {
  constructor() {
    super();
    this.state = {
      goods: [
        {
          id: 1,
          name: '罗拉玫瑰手表',
          imageUrl:
            'https://img12.360buyimg.com/n7/jfs/t1/221495/20/17187/479673/628357bcE452858b5/092540796a65677c.jpg',
          price: 1299,
          num: 1,
        },
        {
          id: 2,
          name: 'SKII神仙水',
          imageUrl:
            'https://img11.360buyimg.com/n7/jfs/t1/4441/20/17316/90607/6283a6e2E6f2d4843/96828b4b7d47e170.jpg',
          price: 699,
          num: 1,
        },
        {
          id: 3,
          name: '德芙巧克力',
          imageUrl:
            'https://img11.360buyimg.com/n7/jfs/t1/207596/17/3697/194769/615ba7e0Ed230a167/63c62dc8e3e3badf.jpg',
          price: 99,
          num: 1,
        },
        {
          id: 4,
          name: '99朵玫瑰',
          imageUrl:
            'https://img10.360buyimg.com/n7/jfs/t1/169228/26/15467/218225/6062ff66E2a840ad4/5b8c988e740845d6.jpg',
          price: 299,
          num: 1,
        },
      ],
    };
  }
  
  render() {
    return (
      <div>
        <h2>父组件</h2>

        <hr />

        <Child {...this.state} delItem={(i) => this.delItem(i)}></Child>
      </div>
    );
  }

  // 根据下标删除一项
  delItem(i) {
    let { goods } = this.state; // 解构出数组

    goods.splice(i, 1); // 删除一项
    this.setState({ goods }); // 通过setState修改状态机
  }
}
