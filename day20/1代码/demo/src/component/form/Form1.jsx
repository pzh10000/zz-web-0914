import React, { Component } from 'react';

export default class Form1 extends Component {
  constructor() {
    super();

    this.state = {
      // 1、声明一个变量，用于获取DOM元素，在这里，变量值为 {current: null}
      box: React.createRef(null),
    };
  }

  render() {
    // console.log(this.state.box); // {current: null}

    let { box } = this.state;
    return (
      <div>
        <h1>非受控组件</h1>

        <p>
          图片上传：
          {/* 2、和具体的DOM节点绑定 */}
          <input type="file" ref={box} />
        </p>

        <p>
          <button onClick={() => this.submit()}>上传</button>
        </p>
      </div>
    );
  }

  submit() {
    // 3、取得DOM节点，取得真正的上传的数据，赋给某一个变即可
    console.log(this.state.box.current.files[0]);
  }
}
