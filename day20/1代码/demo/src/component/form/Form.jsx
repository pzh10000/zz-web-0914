import React, { Component } from 'react';
import './form.css';

export default class Form extends Component {
  constructor() {
    super();
    this.state = {
      username: '小王', // 姓名
      con: '今天天气真好', // 留言
      sex: 1, // 单选框  1男   2女
      ah: ['学习'], // 复选框
      job: 'UI', // 工作种类   前端  后端  测试 UI
    };
  }

  render() {
    let { username, con, sex, ah, job } = this.state;
    return (
      <div className="container">
        <h1>受控组件</h1>

        <p>
          {/*  单行文本框:username */}
          姓名：
          <input
            type="text"
            value={username}
            onChange={(e) => this.setState({ username: e.target.value })}
          />
        </p>

        <p>
          {/* 多行文本域 */}
          留言：
          <textarea
            cols="60"
            rows="5"
            value={con}
            onChange={(e) => this.setState({ con: e.target.value })}
          ></textarea>
        </p>

        <p>
          {/* 单选框 */}
          性别：
          <label>
            <input
              type="radio"
              value={1}
              checked={sex === 1}
              onChange={(e) => this.setState({ sex: e.target.value / 1 })}
            />
            男
          </label>
          <label>
            <input
              type="radio"
              value={2}
              checked={sex === 2}
              onChange={(e) => this.setState({ sex: e.target.value / 1 })}
            />
            女
          </label>
        </p>

        <p>
          爱好：
          <label>
            <input
              type="checkbox"
              value={'学习'}
              checked={ah.includes('学习')}
              onChange={(e) => this.changeAh(e)}
            />
            学习
          </label>
          <label>
            <input
              type="checkbox"
              value={'烫头'}
              checked={ah.includes('烫头')}
              onChange={(e) => this.changeAh(e)}
            />
            烫头
          </label>
          <label>
            <input
              type="checkbox"
              value={'抽烟'}
              checked={ah.includes('抽烟')}
              onChange={(e) => this.changeAh(e)}
            />
            抽烟
          </label>
          <label>
            <input
              type="checkbox"
              value={'喝酒'}
              checked={ah.includes('喝酒')}
              onChange={(e) => this.changeAh(e)}
            />
            喝酒
          </label>
        </p>

        <p>
          工作种类：
          <select
            value={job}
            onChange={(e) => this.setState({ job: e.target.value })}
          >
            <option value="" disabled>
              请选择
            </option>
            <option>前端</option>
            <option>后端</option>
            <option>测试</option>
            <option>UI</option>
          </select>
        </p>

        <p>
          <button onClick={() => this.submit()}>提交</button>
        </p>
      </div>
    );
  }

  changeAh(e) {
    let { ah } = this.state; // 爱好数组
    let val = e.target.value; // 当前item项的值

    let index = ah.indexOf(val); // 取得下标
    if (index == -1) {
      // 没有
      ah.push(val);
    } else {
      // 有了就删除
      ah.splice(index, 1);
    }

    this.setState({ ah });
  }

  submit() {
    console.log(this.state);
  }
}
