import Vue from 'vue';

import vTitle from './vTitle.vue';
import vSearch from './vSearch.vue';

let obj = {
  vTitle,
  vSearch
};

for (let attr in obj) {
  Vue.component(attr, obj[attr]);
}
