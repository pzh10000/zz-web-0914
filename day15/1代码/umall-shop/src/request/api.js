import { get, post } from './index';

// 会员注册（vue项目）
export const register = (data) => post('/register', data);
// 会员登录（vue项目）
export const login = (data) => post('/login', data);

// 获取一级分类信息(首页快速分类导航)
export const getcate = () => get('/getcate');
// 获取轮播图信息(首页)
export const getbanner = () => get('/getbanner');
// 获取商品信息(首页)
export const gethortgoods = () => get('/gethortgoods');


// 分类
// 获取商品全部分类信息(全部分类，数据呈递归树状)
export const getcates = () => get('/getcates');

// 列表
// 获取二级分类下的所有商品
export const getgoodlist = (data) => get('/getgoodlist', data);
