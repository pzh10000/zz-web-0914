import { menulist } from '../../../request/api';

export default {
  state: {
    menuList: [],
  },
  getters: {
    get_menuList(state) {
      return state.menuList;
    },
  },
  mutations: {
    mutation_menuList(state, payload) {
      state.menuList = payload;
    },
  },
  actions: {
    action_menuList({ commit }) {
      // 获取菜单列表
      menulist()
        .then((res) => {
          // console.log(res);
          if (res.code == 200) {
            commit('mutation_menuList', res.list);
          }
        })
        .catch((err) => {
          console.log(err);
        });
    },
  },
  namespaced: true,
};
