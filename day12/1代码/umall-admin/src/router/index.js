import Vue from 'vue';
import VueRouter from 'vue-router';

import store from '../store'; // 仓库

Vue.use(VueRouter);

// 检查path在menus_url这个数组中是否存在，如果存在放行，不存在则到欢迎界面
function routerUrl(path, next) {
  if (store.state.userInfo.menus_url.includes(path)) {
    next();
  } else {
    next('/home');
  }
}

// 导出二级路由信息，方便外面实现下拉
export let routerArr = [
  {
    path: '/menu',
    component: () => import('../views/menu/menu.vue'),
    meta: {
      title: '菜单管理',
    },
    beforeEnter(to, from, next) {
      // console.log(to.path); // 要去的地方
      // next() // 放行
      routerUrl(to.path, next);
    },
  },
  {
    path: '/role',
    component: () => import('../views/role/role.vue'),
    meta: {
      title: '角色管理',
    },
    beforeEnter(to, from, next) {
      routerUrl(to.path, next);
    },
  },
  {
    path: '/user',
    component: () => import('../views/user/user.vue'),
    meta: {
      title: '管理员管理',
    },
    beforeEnter(to, from, next) {
      routerUrl(to.path, next);
    },
  },
  {
    path: '/cate',
    component: () => import('../views/cate/cate.vue'),
    meta: {
      title: '商品分类',
    },
    beforeEnter(to, from, next) {
      routerUrl(to.path, next);
    },
  },
  {
    path: '/specs',
    component: () => import('../views/specs/specs.vue'),
    meta: {
      title: '商品规格',
    },
    beforeEnter(to, from, next) {
      routerUrl(to.path, next);
    },
  },
];

const routes = [
  {
    path: '/login',
    component: () => import('../pages/login.vue'),
  },
  {
    path: '/index',
    component: () => import('../pages/index.vue'),
    children: [
      {
        path: '/home',
        component: () => import('../views/home.vue'),
      },
      ...routerArr,
      {
        path: '',
        redirect: '/home',
      },
    ],
  },
  {
    path: '*',
    redirect: '/index',
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

// 全局路由导航守卫-前置钩子函数(如果不登录，任何页面都不可以访问)
router.beforeEach((to, from, next) => {
  // 1、如果去的是登录页，放行
  if (to.path == '/login') {
    next();
    return;
  }

  // 2、如果本地存储中有登录信息，放行
  if (store.state.userInfo.username) {
    next();
    return;
  }

  // 3、其它的全部到登录页
  next('/login');
});

export default router;
