import { rolelist } from '../../../request/api';

export default {
  state: {
    roleList: [], // 角色管理列表
  },
  getters: {
    get_roleList(state) {
      return state.roleList;
    },
  },
  mutations: {
    mutation_roleList(state, payload) {
      state.roleList = payload;
    },
  },
  actions: {
    action_roleList({ commit }) {
      rolelist()
        .then((res) => {
          // console.log(res);
          commit('mutation_roleList', res.list);
        })
        .catch((err) => {
          console.log(err);
        });
    },
  },
  namespaced: true,
};
