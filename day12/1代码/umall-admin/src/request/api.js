import { get, post } from './index';

// ============菜单管理==============
// 菜单添加
export const menuadd = (data) => post('/menuadd', data);
// 菜单列表
export const menulist = () => get('/menulist?istree=true');
// 菜单删除
export const menudelete = (data) => post('/menudelete', data);
// 菜单获取（一条）
export const menuinfo = (data) => get('/menuinfo', data);
// 菜单修改
export const menuedit = (data) => post('/menuedit', data);

// ============角色管理==============
// 角色添加 
export const roleadd = (data) => post('/roleadd', data);
// 角色列表 
export const rolelist = () => get('/rolelist');
// 角色删除
export const roledelete = (data) => post('/roledelete', data);
// 角色获取（一条）
export const roleinfo = (data) => get('/roleinfo', data);
// 角色修改
export const roleedit = (data) => post('/roleedit', data);

// ============管理员管理==============
// 管理员添加 
export const useradd = (data) => post('/useradd', data);
// 管理员列表（分页） 
export const userlist = (data) => get('/userlist', data);
// 管理员删除
export const userdelete = (data) => post('/userdelete', data);
// 管理员获取（一条）
export const userinfo = (data) => get('/userinfo', data);
// 管理员修改
export const useredit = (data) => post('/useredit', data);
// 管理员总数（用于计算分页）
export const usercount = () => get('/usercount');
// 管理员登录
export const userlogin = (data) => post('/userlogin', data);

// ============商品分类管理==============
// 商品分类添加 
export const cateadd = (data) => post('/cateadd', data, true);
// 商品分类列表  
export const catelist = (data) => get('/catelist?istree=true', data);
// 商品分类删除
export const catedelete = (data) => post('/catedelete', data);
// 商品分类获取（一条）
export const cateinfo = (data) => get('/cateinfo', data);
// 商品分类修改
export const cateedit = (data) => post('/cateedit', data, true);

// ============商品规格管理==============
// 商品规格添加 
export const specsadd = (data) => post('/specsadd', data);
// 商品规格列表（分页） 
export const specslist = (data) => get('/specslist', data);
// 商品规格删除 
export const specsdelete = (data) => post('/specsdelete', data);
// 商品规格获取（一条）
export const specsinfo = (data) => get('/specsinfo', data);
// 商品规格修改
export const specsedit = (data) => post('/specsedit', data);