import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

// 1、引入路由模式
// 2、将App组件用这个标签包起来
import { HashRouter, BrowserRouter } from 'react-router-dom';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  // HashRouter: 访问 http://localhost:3000/#/
  // BrowserRouter:访问: http://localhost:3000/
  <BrowserRouter>
    <App />
  </BrowserRouter>
);
