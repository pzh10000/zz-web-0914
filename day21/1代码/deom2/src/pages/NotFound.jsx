import React from 'react';

export default function NotFound() {
  return (
    <div>
      <h1>你访问的页面飞走了</h1>
    </div>
  );
}
