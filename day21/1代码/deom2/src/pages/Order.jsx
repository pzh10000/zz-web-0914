import React from 'react';

import { useNavigate } from 'react-router-dom';

export default function Order() {
  // useNavigate：必须在函数组件内调用，调用之后，返回的还是一个函数，该函数接收两个参数，第一个to，即跳转到地址，第二个options对象，即传的其它的配置

  let navigate = useNavigate(); // 编程式导航函数
  
  return (
    <div>
      <h1>订单</h1>

      {/* 编程式导航 */}
      <p>编程式导航：点击跳到详情</p>

      {/* 相当于vue的push */}
      <p>
        <button onClick={() => navigate('/detail')}>去详情</button>
      </p>

      <p>
        {/* 新的页面历史记录替换当前的页面历史记录 */}
        <button onClick={() => navigate('/detail', { replace: true })}>
          去详情（替换历史记录）
        </button>
      </p>
    </div>
  );
}
