import React from 'react';
import { useParams } from 'react-router-dom';

export default function List() {
  // console.log(useParams());
  let user = useParams(); // 获取传过来的动态路由参数，它是一个对象
  return (
    <div>
      <h1>列表</h1>
      <p>获取到的参数是</p>
      <p>姓名：{user.name}</p>
      <p>密码：{user.pass}</p>
    </div>
  );
}
