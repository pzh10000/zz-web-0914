import React from 'react';
import { useNavigate, useLocation } from 'react-router-dom';

export default function Detail() {
  let navigate = useNavigate();

  // console.log(useLocation()); // 它调用返回一个对象，相当于vue中的$route，它里面有state属性
  let { state } = useLocation();
  console.log(state);
  return (
    <div>
      <h1>详情</h1>
      <p>取得的数据是</p>
      <p>姓名：{state.name}</p>
      <p>密码：{state.pass}</p>

      <hr />
      <p>
        <button onClick={() => navigate(-1)}>返回</button>
      </p>
    </div>
  );
}
