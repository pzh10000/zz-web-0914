import React from 'react';

import { useSearchParams } from 'react-router-dom';

export default function Search() {
  // console.log(useSearchParams()); // 这个方法调用会返回一个数组，数组第一项是一个map对象，第二项是个函数，用于修改第一个对象的

  let [user] = useSearchParams(); // [数据对象, 修改对象的方法]
  // 拿到对象之后，通过get方法取值

  console.log(user);
  console.log(user.get('name')); // zs
  console.log(user.get('pass')); // 123
  return (
    <div>
      <h1>搜索</h1>

      <p>接收到传过来的查询字符串参数</p>
      <p>用户名：{user.get('name')}</p>
      <p>密码：{user.get('pass')}</p>
    </div>
  );
}
