import React from 'react';

import { Outlet, NavLink } from 'react-router-dom';

export default function Index() {
  return (
    <div>
      {/* <h1>Index</h1> */}

      {/* 二级路由出口，相当于vue中的router-view */}
      <Outlet></Outlet>

      <div className="navBar">
        <NavLink to="/index/home">home</NavLink>
        <NavLink to="/index/sort">分类</NavLink>
        <NavLink to="/index/cart">购物车</NavLink>
        <NavLink to="/index/user">我的</NavLink>
      </div>
    </div>
  );
}
