import React from 'react';
import './App.css';
import { Routes, Route, Navigate, Link, NavLink } from 'react-router-dom';
import PrivateRoute from './tools/PrivateRoute';

// 第一步：定义组件（组件都定义成函数组件）

// 第二步：引入组件(直接引入)
// import Index from './pages/Index';
// import Order from './pages/Order';
// import Search from './pages/Search';
// import Detail from './pages/Detail';
// import List from './pages/List';
// import NotFound from './pages/NotFound'; // 引入404页面

// 使用路由懒加载
let Index = React.lazy(() => import('./pages/Index'));
let Order = React.lazy(() => import('./pages/Order'));
let Search = React.lazy(() => import('./pages/Search'));
let Detail = React.lazy(() => import('./pages/Detail'));
let List = React.lazy(() => import('./pages/List'));
let Login = React.lazy(() => import('./pages/Login'));
let NotFound = React.lazy(() => import('./pages/NotFound'));

let Home = React.lazy(() => import('./views/Home'));
let Sort = React.lazy(() => import('./views/Sort'));
let Cart = React.lazy(() => import('./views/Cart'));
let User = React.lazy(() => import('./views/User'));

let isLogin = false; // 代表是否登录了
export default function App() {
  return (
    <React.Suspense fallback={<h3>加载中...</h3>}>
      <div>
        {/* 路由导航方式一：Link，必须要有to属性，即跳转到哪个url地址。都解析成a标签 */}
        {/* Link不能设置高亮 */}
        {/* <div className="title">
        <Link to="/index">首页</Link>
        <Link to="/order">订单</Link>
        <Link to="/search">搜索</Link>
        <Link to="/detail">详情</Link>
        <Link to="/list">列表</Link>
      </div> */}

        {/* NavLink：可以设置高亮 */}
        {/* 高亮类有三种方式： */}
        {/* 方式一：默认高亮类：active */}
        <div className="title">
          <NavLink to="/index">首页</NavLink>
          <NavLink to="/order">订单</NavLink>
          {/* 方式一：传查询字符串 */}
          <NavLink to="/search?name=zs&pass=123">搜索</NavLink>
          {/* 方式二：传state对象 */}
          <NavLink to="/detail" state={{ name: 'zs', pass: 333 }}>
            详情
          </NavLink>
          <NavLink to="/list/zs/123">列表</NavLink>
        </div>

        {/* 方式二：改自定义的class类，className对应一个回调函数，回调函数的参数是一个对象，里面有一个isActive的属性，如果选中，该属性为真，否则为假 */}
        {/* <div className="title">
        <NavLink
          to="/index"
          className={({ isActive }) => (isActive ? 'sel' : null)}
        >
          首页
        </NavLink>
        <NavLink
          to="/order"
          className={({ isActive }) => (isActive ? 'sel' : null)}
        >
          订单
        </NavLink>
        <NavLink
          to="/search"
          className={({ isActive }) => (isActive ? 'sel' : null)}
        >
          搜索
        </NavLink>
        <NavLink
          to="/detail"
          className={({ isActive }) => (isActive ? 'sel' : null)}
        >
          详情
        </NavLink>
        <NavLink
          to="/list"
          className={({ isActive }) => (isActive ? 'sel' : null)}
        >
          列表
        </NavLink>
      </div> */}

        {/* 方式三：用style，原理同第二种 */}
        {/* <div className="title">
        <NavLink to="/index" style={({isActive})=> isActive ? {background: 'green'}:null}>首页</NavLink>
        <NavLink to="/order" style={({isActive})=> isActive ? {background: 'green'}:null}>订单</NavLink>
        <NavLink to="/search" style={({isActive})=> isActive ? {background: 'green'}:null}>搜索</NavLink>
        <NavLink to="/detail" style={({isActive})=> isActive ? {background: 'green'}:null}>详情</NavLink>
        <NavLink to="/list" style={({isActive})=> isActive ? {background: 'green'}:null}>列表</NavLink>
      </div> */}

        {/* 第三步：路由出口，即组件视图展示在这里 */}
        <Routes>
          {/* 第四步：路由规则，它有path和element，定义url和组件 */}
          <Route path="/index" element={<Index></Index>}>
            {/* 设置二级路由规则 */}
            <Route path="home" element={<Home></Home>}></Route>
            <Route path="sort" element={<Sort></Sort>}></Route>

            {/* 路由导航守卫方式一：直接在标签上写，检查是否有isLogin，如果有，正常展示组件，如果没有，则重定向到登录 */}
            <Route
              path="cart"
              element={
                isLogin ? (
                  <Cart></Cart>
                ) : (
                  <Navigate to="/login" replace></Navigate>
                )
              }
            ></Route>

            {/* 路由导航守卫方式二：用一个函数 */}
            <Route
              path="user"
              element={
                <PrivateRoute>
                  <User></User>
                </PrivateRoute>
              }
            ></Route>
            {/* 二级路由出重定 */}
            <Route
              path=""
              element={<Navigate to="/index/home"></Navigate>}
            ></Route>
          </Route>
          <Route path="/order" element={<Order></Order>}></Route>
          <Route path="/search" element={<Search></Search>}></Route>
          <Route path="/detail" element={<Detail></Detail>}></Route>
          <Route path="/list/:name/:pass" element={<List></List>}></Route>
          <Route path="/login" element={<Login></Login>}></Route>

          {/* 重定向，当访问/时，让它跳转到/index */}
          {/* Navigate重定向 */}
          {/* 这里可写成*，则可以不用配404，只要路由匹配不成功，都重定向 */}
          <Route path="/" element={<Navigate to="/index"></Navigate>}></Route>

          {/* 配置404路由，从上到下，没有匹配上，就匹配到* */}
          <Route path="*" element={<NotFound></NotFound>}></Route>
        </Routes>
      </div>
    </React.Suspense>
  );
}
