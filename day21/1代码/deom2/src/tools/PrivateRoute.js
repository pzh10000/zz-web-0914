import { Navigate } from 'react-router-dom';

export default function Private(props) {
  // props.children 是被<PrivateRoute></PrivateRoute>嵌套的组件
  let isLogin = false; // 是否登录了
  return isLogin ? props.children : <Navigate to={'/login'} replace></Navigate>;
}
