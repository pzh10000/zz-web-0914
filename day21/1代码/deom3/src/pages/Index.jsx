import React from 'react';
import './index.css'

import { Outlet, NavLink } from 'react-router-dom';

export default function Index() {
  return (
    <div>
      {/* 二级路由出口 */}
      <Outlet></Outlet>

      <div className="footBar">
        <NavLink to="/index/home">home</NavLink>
        <NavLink to="/index/sort">分类</NavLink>
        <NavLink to="/index/cart">购物车</NavLink>
        <NavLink to="/index/user">我的</NavLink>
      </div>
    </div>
  );
}
