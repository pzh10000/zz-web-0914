import React, { Suspense, lazy } from 'react';
import { Routes, Route, Navigate } from 'react-router-dom';

import PrivateRoute from './tools/PrivateRoute'; // 路由权限

// 懒加载引入组件
let Index = lazy(() => import('./pages/Index'));
let Detail = lazy(() => import('./pages/Detail'));
let List = lazy(() => import('./pages/List'));
let Order = lazy(() => import('./pages/Order'));
let Search = lazy(() => import('./pages/Search'));
let Login = lazy(() => import('./pages/Login'));

let Home = lazy(() => import('./views/Home'));
let Cart = lazy(() => import('./views/Cart'));
let Sort = lazy(() => import('./views/Sort'));
let User = lazy(() => import('./views/User'));

export default function App() {
  return (
    <Suspense fallback={<h1>加载中...</h1>}>
      <>
        {/* 路由出口 */}
        <Routes>
          {/* 一级路由规则 */}
          <Route path="/index" element={<Index />}>
            {/* 二级路由规则 */}
            <Route path="home" element={<Home />}></Route>
            <Route path="sort" element={<Sort />}></Route>

            {/* 设置路由权限 */}
            <Route path="cart" element={<PrivateRoute><Cart /></PrivateRoute>}></Route>
            <Route path="user" element={<PrivateRoute><User /></PrivateRoute>}></Route>

            {/* 二级路由重定向 */}
            <Route
              path=""
              element={<Navigate to="/index/home"></Navigate>}
            ></Route>
          </Route>
          <Route path="/detail" element={<Detail />}></Route>
          <Route path="/list" element={<List />}></Route>
          <Route path="/order" element={<Order />}></Route>
          <Route path="/search" element={<Search />}></Route>
          <Route path="/login" element={<Login />}></Route>

          {/* 重定向 */}
          <Route path="*" element={<Navigate to="/index"></Navigate>}></Route>
        </Routes>
      </>
    </Suspense>
  );
}
