import React from 'react';
import { Link } from 'react-router-dom';

export default function Home() {
  return (
    <div>
      <h1>Home</h1>

      <ul>
        <li>
          <Link to="/detail">详情</Link>
        </li>
        <li>
          <Link to="/list">列表</Link>
        </li>
        <li>
          <Link to="/order">订单</Link>
        </li>
        <li>
          <Link to="/search">搜索</Link>
        </li>
      </ul>
    </div>
  );
}
