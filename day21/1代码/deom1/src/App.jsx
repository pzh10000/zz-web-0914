import React from 'react';

// 1、作业
// import Zy1 from './component/zy1/Index';

// 2、高阶组件
// import HocIndex from './component/hoc/Index';

// 3、单元素过渡动画
// import Box1 from './component/transition/Box1';
// 4、列表元素动画效果
// import Box2 from './component/transition/Box2';
// 5、开头效果动画
import Box3 from './component/transition/Box3';


export default function App() {
  return (
    <div className="container">
      {/* <Zy1></Zy1> */}
      {/* <HocIndex></HocIndex> */}
      {/* <Box1></Box1> */}
      {/* <Box2></Box2> */}
      <Box3></Box3>
    </div>
  );
}
