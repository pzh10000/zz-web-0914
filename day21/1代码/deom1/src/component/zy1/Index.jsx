import React, { Component } from 'react';

import Edit from './Edit';
import List from './List';

export default class Index extends Component {
  constructor() {
    super();
    this.state = {
      // 列表数据
      list: [
        {
          id: 1,
          name: '小王', // 昵称
          com: '今天中午吃啥', // 留言
        },
        {
          id: 2,
          name: '小芳', // 昵称
          com: '吃包子', // 留言
        },
        {
          id: 3,
          name: '小二', // 昵称
          com: '开酒馆', // 留言
        },
      ],
    };
  }

  // 添加一项（子传父）
  toPar(obj) {
    // console.log(obj); // 子组件传过来的数据
    let { list } = this.state;
    list.unshift({
      ...obj,
      id: Math.random(),
    });
    this.setState({ list });
  }

  // 删除某一项（子传父）
  del(i) {
    // console.log(i); // 要删除的哪一项的下标
    let { list } = this.state;
    list.splice(i, 1);
    this.setState({ list });
  }

  render() {
    let { list } = this.state;
    return (
      <div>
        <h1>父组件</h1>
        <hr />
        <Edit toPar={(obj) => this.toPar(obj)}></Edit>
        <List list={list} del={(i) => this.del(i)}></List>
      </div>
    );
  }
}
