import React, { Component } from 'react';

export default class Edit extends Component {
  constructor() {
    super();
    this.state = {
      // 用户的数据
      user: {
        name: '',
        com: '',
      },
    };
  }

  // 修改state中的user数据
  changeUser(e, attr) {
    // console.log(e.target.value, attr);

    let { user } = this.state; // 取得对象
    user = {
      ...user, // 因为修改name，则com不修改。修改com，则name的值不变
      [attr]: e.target.value,
    };
    this.setState({ user });
  }

  // 提交的时候
  submit() {
    let { toPar } = this.props; // 父组件中传过来的方法体
    // console.log(this.state.user);
    toPar(this.state.user); // 父组件中的方法体被调用，并传参

    // 清空页面中的输入
    this.setState({
      user: {
        name: '',
        com: '',
      },
    });
  }

  render() {
    let { user } = this.state;

    return (
      <div className="alert alert-info">
        <div className="form-group">
          <input
            type="text"
            className="form-control"
            placeholder="请输入昵称"
            value={user.name}
            onChange={(e) => this.changeUser(e, 'name')}
          />
        </div>

        <div className="form-group">
          <textarea
            className="form-control"
            placeholder="请输入留言内容"
            value={user.com}
            onChange={(e) => this.changeUser(e, 'com')}
          ></textarea>
        </div>

        <button
          type="submit"
          className="btn btn-default"
          onClick={() => this.submit()}
        >
          提交留言
        </button>
      </div>
    );
  }
}
