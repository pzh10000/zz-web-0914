import React, { Component } from 'react';

export default class List extends Component {
  render() {
    let { list, del } = this.props; // 获取传过来的数据

    return (
      <div>
        {list.map((item, index) => (
          <div
            key={item.id}
            className={
              index % 2
                ? 'alert-warning alert alert-dismissible'
                : 'alert-danger alert alert-dismissible'
            }
            role="alert"
          >
            <button
              type="button"
              className="close"
              data-dismiss="alert"
              aria-label="Close"
              onClick={() => del(index)}
            >
              <span aria-hidden="true">&times;</span>
            </button>

            <p>昵称：{item.name}</p>
            <p>留言：{item.com}</p>
          </div>
        ))}
      </div>
    );
  }
}
