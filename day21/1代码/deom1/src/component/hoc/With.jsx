import React, { Component } from 'react';

export default class With extends Component {
  render() {
    let { data } = this.props;
    return (
      <div>
        <h1>中午吃啥？</h1>
        <ul>
          {data.map((item) => (
            <li key={item.id}>{item.name}</li>
          ))}
        </ul>
      </div>
    );
  }
}
