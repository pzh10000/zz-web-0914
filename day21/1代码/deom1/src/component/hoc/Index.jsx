import React, { Component } from 'react';

import Hoc from './Hoc'; // 引入高阶组件
import With from './With'; // 引入模板
import data from './data'; // 引入数据
let NewComponent = Hoc(With, data); // NewComponent即产生的新的组件，可以在下面使用

export default class Index extends Component {
  render() {
    return (
      <div>
        <h1>使用hoc高阶组件</h1>

        {/* 使用高阶组件产生的新的组件 */}
        <NewComponent></NewComponent>
      </div>
    );
  }
}
