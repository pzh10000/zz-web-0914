import React from 'react';

// 封装高阶组件，它是一个函数并默认导出，该函数接收两个参数，函数调用返回一个新的类组件
// 参数一：WrappedComponent （模板组件）
// 参数二：data （数据，数据通过自定义属性传到模板组件中）
// 返回值：NewComponent （新的类组件）

export default function (WrappedComponent, data) {
  return class extends React.Component {
    render() {
      return (
        <div>
          <WrappedComponent data={data}></WrappedComponent>
        </div>
      );
    }
  };
}
