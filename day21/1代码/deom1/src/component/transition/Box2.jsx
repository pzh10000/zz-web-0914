import React, { Component } from 'react';
import { CSSTransition, TransitionGroup } from 'react-transition-group';
import './box.css';

export default class Box2 extends Component {
  constructor() {
    super();
    this.state = {
      list: [
        {
          id: 1,
          name: '红烧肉',
        },
        {
          id: 2,
          name: '清蒸大虾',
        },
        {
          id: 3,
          name: '红烧狮子头',
        },
      ],
    };
  }

  // 删除某一项
  del(i) {
    // console.log(i);
    let { list } = this.state;
    list.splice(i, 1);
    this.setState({ list });
  }

  render() {
    let { list } = this.state;
    return (
      <div>
        <h1>TransitionGroup实现列表元素动画效果</h1>

        <hr />
        <ul>
          <TransitionGroup>
            {list.map((item, index) => (
              <CSSTransition
                key={item.id}
                unmountOnExit
                in={true}
                timeout={500}
                classNames="abc"
              >
                <li>
                  {item.name}
                  <button onClick={() => this.del(index)}>删除</button>
                </li>
              </CSSTransition>
            ))}
          </TransitionGroup>
        </ul>
      </div>
    );
  }
}
