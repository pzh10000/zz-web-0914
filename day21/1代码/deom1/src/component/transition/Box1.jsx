import React, { Component } from 'react';

import { CSSTransition } from 'react-transition-group'; // 引入过渡动画

import './box.css';

export default class Box1 extends Component {
  constructor() {
    super();
    this.state = {
      isShow: true, // 盒子的显示状态，true为显示，false为隐藏
      title: '隐藏', // 显示/隐藏
    };
  }
  render() {
    let { isShow, title } = this.state;
    return (
      <>
        <h1>CSSTransition实现单元素过渡动画</h1>

        <hr />

        {/* <h1>仅显示和隐藏</h1>
        <p>
          <button onClick={() => this.setState({ isShow: !isShow })}>
            按钮
          </button>
        </p>
        {isShow ? <div className="box"></div> : null} */}

        <h1>使用动画显示和隐藏</h1>
        <p>
          <button onClick={() => this.setState({ isShow: !isShow })}>
            {title}
          </button>
        </p>

        <CSSTransition
          unmountOnExit
          in={isShow}
          timeout={500}
          classNames="abc"
          onEntered={() => this.setState({ title: '隐藏' })}
          onExited={() => this.setState({ title: '显示' })}
        >
          <div className="box"></div>
        </CSSTransition>
      </>
    );
  }
}
