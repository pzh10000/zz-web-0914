import React, { Component } from 'react';
import './box.css';
import { SwitchTransition, CSSTransition } from 'react-transition-group';

export default class Box3 extends Component {
  constructor() {
    super();
    this.state = {
      isShow: true, // 开关显示
    };
  }
  render() {
    let { isShow } = this.state;
    return (
      <div>
        <h1>SwitchTransition实现开关切换效果</h1>

        <hr />

        <SwitchTransition mode="in-out">
          <CSSTransition
            key={isShow ? 'on' : 'off'}
            classNames="vv"
            timeout={500}
          >
            <button onClick={() => this.setState({ isShow: !isShow })}>
              {isShow ? 'on' : 'off'}
            </button>
          </CSSTransition>
        </SwitchTransition>
      </div>
    );
  }
}
