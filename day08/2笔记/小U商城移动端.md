## 小U商城移动端



### 十七、实现轮播图数据的动态渲染

在vSwiper.vue中，使用全局引入axios，发起get请求，获取banner数据

```
http://localhost:3000/api/getbanner
```

但是此时会报跨域错误

解决方法：项目根目录下，创建vue.config.js配置，前端解决跨域问题





注意，把数据取回来了之后，要在 $nextTick() 方法中或者 updated() 生命周期钩子函数中实例化swiper

理解：nextTick()，是将回调函数延迟在下一次dom更新数据后调用**，简单的理解是：**当数据更新了，在dom中渲染后，自动执行该函数

```vue
<template>
  <div class="swiper-container">
    <div class="swiper-wrapper">
      <div class="swiper-slide" v-for="item in bannerList" :key="item.id">
        <img :src="item.img" :alt="item.title" />
      </div>
    </div>
    <!-- 如果需要分页器 -->
    <div class="swiper-pagination"></div>
  </div>
</template>

<script>
import Swiper from "swiper"; // 注意引入的是Swiper
import "swiper/css/swiper.min.css"; // 注意这里的引入
export default {
  data() {
    return {
      bannerList: [], // 轮播图数据，当我们后面请求了数据之后，再动态展示
    };
  },
  mounted() {
    // 加载完成，请求数据
    this.$axios
      .get("/api/getbanner") // 这里用了代理，所以/代表着代理地址
      .then((res) => {
        if (res.data.code === 200) {
          this.bannerList = res.data.list; // 将请求到的数据，赋给data中的属性
          // 但是这个时候会产生swiper的问题，banner不能滚动了，因为swiper创建实例的时候，banner还没有数据呢
          // 有两种方法解决：
          // 1、swiper的实例创建在this.$nextTick()中，nextTick即下一次dom创建完成之后调用回调函数
          // 2、swiper的实例创建在updated钩子函数中

          // 方式一：swiper的实例创建在this.$nextTick()中
          // this.$nextTick(() => {
          //   // 实例化轮播
          //   new Swiper(".swiper-container", {
          //     loop: true, // 无缝
          //     autoplay: true, // 自动
          //     // 小圆点
          //     pagination: {
          //       el: ".swiper-pagination",
          //     },
          //   });
          // });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  },

  // 方式二：swiper的实例创建在updated的钩子函数中
  updated() {
    // 实例化轮播
    new Swiper(".swiper-container", {
      loop: true, // 无缝
      autoplay: true, // 自动
      // 小圆点
      pagination: {
        el: ".swiper-pagination",
      },
    });
  },
};
</script>

<style scoped>
.swiper-container {
  width: 100%;
  height: 3rem;
}

.swiper-slide img {
  display: block;
  width: 100%;
  height: 100%;
  object-fit: cover;
}
</style>
```











### 十八、实现首页商品列表动态展示

![image-20220522142248282](img/image-20220522142248282.png)

请求数据：http://localhost:3000/api/gethortgoods

```vue
<template>
  <div>
    <!-- 使用轮播图组件 -->
    <v-swiper></v-swiper>

    <!-- 首页商品切换 -->
    <div class="goods">
      <span @click="index = 1" :class="{ active: index == 1 }">热门推荐</span>
      <span @click="index = 2" :class="{ active: index == 2 }">上新推荐</span>
      <span @click="index = 3" :class="{ active: index == 3 }">所有商品</span>
    </div>

    <!-- 商品列表展示，应该有三块 -->
    <ul class="goodsUl" v-show="index == 1">
      <li v-for="item in goodsList" :key="item.id" @click="goDetail(item.id)">
        <img :src="item.img" :alt="item.goodsname" />
        <div>
          <h3>{{ item.goodsname }}</h3>
          <span>现价格：￥{{ item.price | toPrice(2) }}</span>
          <del>原价格：￥{{ item.market_price | toPrice(2) }}</del>
          <button>立即抢购</button>
        </div>
      </li>
    </ul>
    <ul class="goodsUl" v-show="index == 2">
      <li v-for="item in hotList" :key="item.id" @click="goDetail(item.id)">
        <img :src="item.img" :alt="item.goodsname" />
        <div>
          <h3>{{ item.goodsname }}</h3>
          <span>现价格：￥{{ item.price | toPrice(2) }}</span>
          <del>原价格：￥{{ item.market_price | toPrice(2) }}</del>
          <button>立即抢购</button>
        </div>
      </li>
    </ul>
    <ul class="goodsUl" v-show="index == 3">
      <li v-for="item in newList" :key="item.id" @click="goDetail(item.id)">
        <img :src="item.img" :alt="item.goodsname" />
        <div>
          <h3>{{ item.goodsname }}</h3>
          <span>现价格：￥{{ item.price | toPrice(2) }}</span>
          <del>原价格：￥{{ item.market_price | toPrice(2) }}</del>
          <button>立即抢购</button>
        </div>
      </li>
    </ul>
  </div>
</template>
<script>
import vSwiper from "../components/vSwiper.vue";
export default {
  data() {
    return {
      index: 1, // 默认展示的第几项
      hotList: [], // 热门推荐
      newList: [], // 上新推荐
      goodsList: [], // 所有商品
    };
  },
  components: {
    vSwiper,
  },
  methods: {
    // 点击跳转到商品详情
    goDetail(id) {
      this.$router.push("/detail/" + id);
    },
  },
  mounted() {
    // 请求首页商品列表数据
    this.$axios
      .get("/api/gethortgoods")
      .then((res) => {
        // console.log(res);
        if (res.data.code === 200) {
          this.hotList = res.data.list[0].content;
          this.newList = res.data.list[0].content;
          this.goodsList = res.data.list[0].content;
        }
      })
      .catch((err) => {
        console.log(err);
      });
  },
};
</script>
<style scoped>
.goods {
  height: 0.8rem;
  display: flex;
}
.goods span {
  flex: 1;
  text-align: center;
  line-height: 0.8rem;
  font-size: 0.25rem;
}
.active {
  background: yellow;
}

.goodsUl {
  width: 96%;
  margin: 0 auto;
  margin-bottom: 1rem;
}
.goodsUl li {
  border: 1px solid #ccc;
  padding: 10px;
  display: flex;
  margin-bottom: 0.16rem;
}
.goodsUl li img {
  border: 1px solid #ccc;
  border-radius: 4px;
  width: 2.2rem;
  height: 2.2rem;
}
.goodsUl li div {
  flex: 1;
  margin-left: 0.2rem;
}
.goodsUl li div h3 {
  font-size: 0.34rem;
  font-weight: bold;
  margin-bottom: 0.2rem;
}
.goodsUl li div span,
.goodsUl li div del {
  font-size: 0.24rem;
  display: block;
  margin-bottom: 0.16rem;
}
.goodsUl li div span {
  color: red;
}
.goodsUl li div del {
  color: #ccc;
}
</style>
```







### 十九、实现注册

* 创建注册组件（视图）

register.vue

进行非空的判断，发起请求，成功之后跳转到登录页

```vue
<template>
  <div>
    <div class="f20">
      <h1 class="m20">注册</h1>
      <p class="m20">
        手机号：
        <input type="text" v-model="user.phone" />
      </p>
      <p class="m20">
        昵称：
        <input type="text" v-model="user.nickname" />
      </p>
      <p class="m20">
        密码：
        <input type="text" v-model="user.password" />
      </p>
      <p class="m20">
        <button @click="register">注册</button>
      </p>
    </div>
  </div>
</template>
<script>
export default {
  data() {
    return {
      user: {
        phone: "",
        nickname: "",
        password: "",
      },
    };
  },
  methods: {
    register() {
      // 非空的判断
      if (!this.user.phone || !this.user.nickname || !this.user.password) {
        alert("请输入必填项");
        return;
      }

      // 可以注册，调用接口
      this.$axios
        .post("/api/register", this.user)
        .then((res) => {
          // console.log(res);
          if (res.data.code === 200) {
            // 注册成功，提示并跳转到登录页。登录页有要实现登录的接口
            // 但是这个时候，一定要取消之前写的全局路由导航
            // 全局路由导航是控制所有页面都必须要登录才可以看
            // 但是这个电商项目只需要控制某些页面需要登录才能看
            alert(res.data.msg);
            this.$router.push("/login");
          } else {
            alert(res.data.msg);
          }
        })
        .catch((err) => {
          console.log(err);
        });
    },
  },
};
</script>
<style scoped>
</style>
```

* 设置注册相关路由

```js
{
    path: '/register',
    component: () => import('../pages/register.vue')
},
```

* 实现注册逻辑（接口的调用）





### 二十、实现登录

```vue
<template>
  <div>
    <h1>登录</h1>
    <div class="m20 p20">
      <p class="mb20">
        <input
          type="text"
          class="p10"
          placeholder="请输入手机号"
          v-model="user.phone"
        />
      </p>
      <p class="mb20">
        <input
          type="text"
          class="p10"
          placeholder="请输入密码"
          v-model="user.password"
        />
      </p>
      <p>
        <button class="p10" @click="login">登录</button>
      </p>
    </div>
  </div>
</template>
<script>
export default {
  data() {
    return {
      user: {
        phone: "",
        password: "",
      },
    };
  },
  methods: {
    login() {
      // 非空的判断
      if (!this.user.phone || !this.user.password) {
        alert("用户名和密码必须填写");
        return;
      }

      // 调用登录接口
      this.$axios
        .post("/api/login", this.user)
        .then((res) => {
          console.log(res);
          if (res.data.code == 200) {
            // 登录成功，跳转到首页，并将返回的数据存入本地存储中，以将来发送token请求
            alert(res.data.msg);
            this.$router.push("/home");
          } else {
            alert(res.data.msg);
          }
        })
        .catch((err) => {
          console.log(err);
        });
    },
  },
};
</script>
<style scoped>
</style>
```





### 二十一、实现商品详情页

request --> api.js中

```js
// 获取商品信息，get方式，有参数。注意get的传参方式
export function getgoodsinfo(data) {
  return http.get('/getgoodsinfo', {
    params: data,
  });
}
```

detail.vue中

```vue
<template>
  <div>
    <h1>商品详情</h1>

    <p class="m20">
      当前商品的id是：
      {{ $route.params.id }}
    </p>

    <!-- 详情内容展示 -->
    <div class="m20">
      <img class="goodsImg" :src="goodsinfo.img" alt="goodsinfo.goodsname" />
      <h3>商品名称：{{ goodsinfo.goodsname }}</h3>
      <p>商品价格：{{ goodsinfo.market_price | toPrice(2) }}</p>
      <div v-if="goodsinfo.description" v-html="goodsinfo.description"></div>
      <div v-else>暂无详情</div>
    </div>
  </div>
</template>
<script>
import { getgoodsinfo } from "../request/api";
export default {
  data() {
    return {
      goodsinfo: {}, // 商品详情对象
    };
  },
  mounted() {
    // 进入组件之后，发起ajax请求数据
    getgoodsinfo({ id: this.$route.params.id })
      .then((res) => {
        // console.log(res);
        if (res.data.code == 200) {
          this.goodsinfo = res.data.list[0];
        }
      })
      .catch((err) => {
        console.log(err);
      });
  },
};
</script>
<style scoped>
.goodsImg {
  width: 100%;
}
</style>
```





### 二十二、实现购物车列表

购物车访问都是需要权限的，这个时候需要验证token，我们在axios中传入token，并可以进行统一错误处理

```vue
<template>
  <div>
    <h1>购物车</h1>
  </div>
</template>

<script>
import { cartlist } from "../request/api";
export default {
  data() {
    return {};
  },
  mounted() {
    // 获取本地存储的token数据，因为请求购物车需要uid
    let userInfo = sessionStorage.getItem("userInfo");
    let uid = userInfo ? JSON.parse(userInfo).uid : null;
    // console.log(uid);
    cartlist({ uid })
      .then((res) => {
        // console.log(res);
        if (res.data.code == 200) {
          console.log(res.data.list);
          console.log(res.data.msg, "---购物车获取成功");
        } else {
          console.log(res.data.msg, "---购物车获取失败");
        }
      })
      .catch((err) => {
        console.log(err);
      });
  },
};
</script>
<style scoped>
</style>
```





### 二十三、实现请求头设置

在axios中做拦截，传入token



### 二十四、做全局的token错误验证

在axios中做拦截，做统一的token错误验证，如果token过期或没有传，让它到登录页面去