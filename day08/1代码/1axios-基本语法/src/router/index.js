import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [
  {
    path: '/axios1',
    component: () => import('../components/axiosDemo'),
  },
  {
    path: '/axios2',
    component: () => import('../components/axiosDemo2'),
  },
  {
    path: '*',
    redirect: '/axios1'
  }
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
