import Vue from 'vue';

import toPrice from './toPrice';
import toUpper from './toUpper';

let obj = {
  toPrice,
  toUpper,
};

for (let attr in obj) {
  Vue.filter(attr, obj[attr]);
}
