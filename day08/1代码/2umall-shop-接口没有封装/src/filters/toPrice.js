export default (price, num = 2) => {
  return price.toFixed(num);
};
