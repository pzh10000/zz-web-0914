module.exports = {
  // devServer 本地开发服务配置
  devServer: {
    // 设置你的代理地址（这里写后端服务器的地址）
    // 以后前端访问服务器地址时，前面直接写 /  ，不用写http://localhost:3000
    // 后端地址本来是：http://localhost:3000/api/getbanner
    // 会换成：http://localhost:8080/api/getbanner
    // 这样就没有跨域问题了
    // proxy即代理
    proxy: 'http://localhost:3000',
  },
};
