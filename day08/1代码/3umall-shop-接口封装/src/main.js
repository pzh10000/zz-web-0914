import Vue from 'vue';
import App from './App.vue';
import router from './router';

Vue.config.productionTip = false;

// 引入rem
import './assets/js/rem';
// 引入重置css
import './assets/css/base.css';

// 引入全局组件
import './common';
// 引入全局过滤器
import './filters';

// 引入font字体图标
import './assets/font/iconfont.css';

// 引入过渡动画
import 'animate.css/animate.css';

new Vue({
  router,
  render: (h) => h(App),
}).$mount('#app');
