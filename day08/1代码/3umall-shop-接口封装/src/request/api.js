import http from './axios';
import qs from 'qs';

// 首页获取轮播图
export function getbanner() {
  return http.get('/getbanner');
}

// 获取商品信息(首页)
export function gethortgoods() {
  return http.get('/gethortgoods');
}

// 获取一级分类信息(首页快速分类导航)
export function getcate() {
  return http.get('/getcate');
}

// 会员注册（vue项目）
export function register(data) {
  return http.post('/register', qs.stringify(data));
}

// 会员登录（vue项目）
export function login(data) {
  return http.post('/login', qs.stringify(data));
}

// 获取一个商品信息
export function getgoodsinfo(data) {
  return http.get('/getgoodsinfo', {
    params: data,
  });
}

// 购物车列表
export function cartlist(data) {
  return http.get('/cartlist', {
    params: data,
  });
}

// 购物车添加
export function cartadd(data) {
  return http.post('/cartadd', qs.stringify(data));
}