import axios from 'axios'; // 引入axios
import router from '../router'; // 为了使用它下面的跳转

// 创建axios实例
let http = axios.create({
  baseURL: '/api',
});

// 请求拦截
http.interceptors.request.use((req) => {
  // console.log(req);
  // req.headers.pzh = '123'; // 携带token

  let userInfo = sessionStorage.getItem('userInfo');
  let token = userInfo ? JSON.parse(userInfo).token : '';
  req.headers.authorization = token;

  return req;
});

// 响应拦截
http.interceptors.response.use((res) => {
  // console.log(res);

  // 作用1：数据过滤
  // return res.data; // 做数据过滤

  // 作用2：全局错误处理
  // 实际开发中，我们应该以code码为准，但是我们这个服务器不成功的都是返回的500，所以我们采用msg来区分
  // console.log(res);

  if (
    res.data.msg == '请设置请求头，并携带验证字符串' ||
    res.data.msg == '登录已过期或访问权限受限'
  ) {
    // 跳转到登录页
    router.push('/login');
    return;
  }

  return res;
});

export default http;
