export default (price, num = 2) => {
  return price ? price.toFixed(num) : '';
};
