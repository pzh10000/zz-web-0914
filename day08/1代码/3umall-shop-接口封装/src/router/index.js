import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [
  // 首页
  {
    path: '/index',
    component: () => import('../pages/index'),
    children: [
      {
        path: '/home',
        component: () => import('../views/home'),
        meta: {
          title: '首页',
        },
      },
      {
        path: '/cart',
        component: () => import('../views/cart'),
        meta: {
          title: '购物车',
        },
      },
      {
        path: '/sort',
        component: () => import('../views/sort'),
        meta: {
          title: '分类',
        },
      },
      {
        path: '/user',
        component: () => import('../views/user'),
        meta: {
          title: '个人中心',
        },
      },
      {
        path: '',
        redirect: '/home',
      },
    ],
  },
  // 详情
  {
    path: '/detail/:id',
    component: () => import('../pages/detail'),
  },
  // 列表
  {
    path: '/list',
    component: () => import('../pages/list'),
  },
  // 订单
  {
    path: '/order',
    component: () => import('../pages/order'),
  },
  // 搜索
  {
    path: '/search',
    component: () => import('../pages/search'),
  },
  {
    path: '/login',
    component: () => import('../pages/login'),
  },
  {
    path: '/register',
    component: () => import('../pages/register'),
  },
  {
    path: '*',
    redirect: '/index',
  },
];

const router = new VueRouter({
  routes,
  // mode: 'hash', // 默认模式，地址上会有#号
  mode: 'history', // 地址没有#号

  // 路由的滚动行为
  scrollBehavior(to, from, position) {
    // console.log(position, '滚动位置');
    // 如果用户滑动了滚轴，那么position就有数据，我们可以直接返回该数据，否则清空位置
    if (position) {
      this.$position = position; // this ==> $router
      return position;
    } else {
      this.$position = { x: 0, y: 0 };
      return { x: 0, y: 0 };
    }
  },
});

export default router;
