import React from 'react';

export default function Child(props) {
  // console.log(props); // 父组件传过来的数据，是一个对象
  return (
    <div className="well">
      <h1>子组件</h1>

      <p>使用父组件传过来的数据</p>
      <p>{props.name}</p>
      <p>{props.t}</p>
    </div>
  );
}
