import React from 'react';

import Child from './Child';

let uname = 'zs';
let txt = '我是小张，我今年5岁，我今天做核酸了';

export default function Fun() {
  
  return (
    <div className="alert alert-success">
      <h1>父组件</h1>
      <p>使用自己的数据</p>
      <p>姓名：{uname}</p>
      <p>自述：{txt}</p>

      {/* 通过自定义属性，向子组件传参 */}
      <Child name={uname} t={txt}></Child>
    </div>
  );
}
