import React, { Component } from 'react';

import Child from './Child';

export default class Props extends Component {
  constructor() {
    super();

    this.state = {
      title: '小王',
      con: '我是小王，会变成老王',
    };
  }
  render() {
    return (
      <div>
        <h1>父组件</h1>
        <p>姓名：{this.state.title}</p>
        <p>描述：{this.state.con}</p>

        <hr />

        {/* 向子组一个一个传数据 */}
        {/* <Child title={this.state.title} con={this.state.con}></Child> */}

        {/* 2、将state / props上所有的数据一次传递给子组件 */}
        <Child {...this.state}></Child>
      </div>
    );
  }
}
