import React, { Component } from 'react';

export default class Child extends Component {
  // 3、在constructor中取得props
  // 需求：我们可以在render中取得this.props，哪么在constructor也能取得不？
  // 在constructor中要取得props，必须传入参数props，并且在super()中也要传入props参数
  constructor(props) {
    super(props);

    console.log(this);
    console.log(this.props);
  }

  render() {
    // console.log(this.props); // {title: '小王', con: '我是小王，会变成老王'}

    // 1、在子组件中，通过解构赋值获取props数据
    let { title, con } = this.props;
    return (
      <div>
        <h2>子组件</h2>
        <p>使用父组件的数据</p>
        <p>{title}</p>
        <p>{con}</p>
      </div>
    );
  }
}
