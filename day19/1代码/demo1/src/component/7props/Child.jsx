import React, { Component } from 'react';

export default class Child extends Component {
  render() {
    let { name, fn } = this.props;
    return (
      <div>
        <h1>子组件</h1>

        <p>{name}</p>

        <p>
          <button onClick={() => fn('小芳')}>
            调用父组件传过来的方法
          </button>
        </p>
      </div>
    );
  }
}
