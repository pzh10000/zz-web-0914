import React, { Component } from 'react';

import Child from './Child';

export default class Props extends Component {
  constructor() {
    super();
    this.state = {
      name: 'zs',
    };
  }

  render() {
    return (
      <div>
        <h1>父组件</h1>
        <p>{this.state.name}</p>

        <hr />

        <Child name={this.state.name} fn={(n) => this.fn(n)}></Child>
      </div>
    );
  }

  fn(n) {
    console.log(n);
    this.setState({ name: n });
  }
}
