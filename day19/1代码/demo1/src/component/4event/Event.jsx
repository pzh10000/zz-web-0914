import React, { Component } from 'react';

export default class Event extends Component {
  render() {
    return (
      <div>
        <h1>事件绑定</h1>

        <p>
          {/* 这种形式的事件绑定不能传参 */}
          <button onClick={this.fn1}>点击我执行一个函数</button>
        </p>

        <hr />

        <p>
          {/* bind事件绑定，可以传参，稍显复杂，也较少使用 */}
          <button onClick={this.fn2.bind(this, 20, 30)}>通过bind绑定</button>
        </p>

        <hr />
        <p>
          {/* 箭头函数绑定事件，即不改this指向，也可以传参，常用 */}
          <button onClick={() => this.fn3(10, 20)}>箭头函数绑定</button>
        </p>
      </div>
    );
  }

  fn1() {
    console.log(this); // undefined
    console.log('我执行了');
  }

  fn2(a, b) {
    console.log(this); // 当前这个类的实例
    console.log('bind绑定');
    console.log(a, b);
  }

  fn3(x, y) {
    console.log(this); // 当前这个类的实例
    console.log('箭头函数绑定');
    console.log(x, y);
  }
}
