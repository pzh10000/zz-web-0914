import React, { Component } from 'react';

import './stop.css';

export default class Stop extends Component {
  render() {
    return (
      <div>
        <h1>阻止默认事件 / 阻止事件传播 / 事件捕获</h1>

        <hr />
        <p>阻止默认事件</p>
        <p>
          <a href="https://baidu.com" onClick={(e) => this.stop(e)}>
            百度
          </a>
        </p>

        <hr />
        <p>阻止事件传播</p>
        <div className="box" onClick={() => this.fn1()}>
          <div className="small" onClick={(e) => this.fn2(e)}></div>
        </div>

        <hr />
        <p>事件捕获</p>
        <div className="box" onClickCapture={() => this.fn1()}>
          <div className="small" onClickCapture={() => this.fn2()}></div>
        </div>
      </div>
    );
  }

  stop(e) {
    // 阻止默认事件
    e.preventDefault();
  }

  fn1() {
    console.log('大盒子');
  }

  fn2(e) {
    // 阻止冒泡
    // e.stopPropagation();
    console.log('小盒子');
  }
}
