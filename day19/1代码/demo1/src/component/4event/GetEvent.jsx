import React, { Component } from 'react';

export default class GetEvent extends Component {
  render() {
    return (
      <div>
        <h1>获取事件对象</h1>

        <p>
          {/* 事件对象放在形参的末尾 */}
          <button onClick={this.fn1.bind(this, 10)}>
            bind绑定获取事件对象
          </button>
        </p>

        <hr />
        <p>
          {/* 事件对象存在于事件函数中，这里箭头函数就是事件函数 */}
          <button onClick={(e) => this.fn2(e, 10, 20)}>
            箭头函数获取事件对象
          </button>
        </p>
      </div>
    );
  }

  // bind绑定的函数，事件对象放在形参的末尾
  fn1(num, e) {
    console.log(num);
    console.log(e);
  }

  // 在箭头函数中，根据实际参数的位置，映射到形参中即可
  fn2(e, a, b) {
    console.log(e);
    console.log(a, b);
  }
}
