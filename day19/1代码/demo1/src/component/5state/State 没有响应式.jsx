import React, { Component } from 'react';

export default class State extends Component {
  // 如此定义的数据，不是响应式的
  uname = '小王';

  render() {
    return (
      <div>
        <h1>State状态机</h1>

        {/* 使用定义的数据 */}
        <p>姓名：{this.uname}</p>

        <p>
          <button onClick={() => this.changeUname()}>修改uname</button>
        </p>
      </div>
    );
  }

  changeUname() {
    this.uname = '老王';
    console.log(this.uname);
  }
}
