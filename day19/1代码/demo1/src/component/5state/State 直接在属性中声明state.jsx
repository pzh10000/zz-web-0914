import React, { Component } from 'react';

export default class State extends Component {
  // 使用state状态机，state是一个对象

  // 直接在属性中声明state(不常用)
  state = {
    uname: '小王',
  };

  render() {
    return (
      <div>
        <h1>State状态机</h1>

        {/* 使用定义的数据 */}
        <p>姓名：{this.state.uname}</p>

        <p>
          <button onClick={() => this.changeUname()}>修改uname</button>
        </p>
      </div>
    );
  }

  changeUname() {
    // 修改state中的数据，必须按一定的规则
    // this.state.uname = '老王'; // 不可以
    // console.log(this.state.uname);

    // ---------------
    // 正确做法，state状态机中的数据，必须使用this.setState来修改
    // console.log(this);

    // 语法：this.setState({被修改State数据中的key: 新的value}, callback)
    this.setState({ uname: '老王' }, () => {
      console.log(this.state.uname);
    });
  }
}
