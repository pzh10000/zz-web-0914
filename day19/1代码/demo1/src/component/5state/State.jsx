import React, { Component } from 'react';

export default class State extends Component {
  // state声明在构造函数中（常用的写法）

  // constructor是构造函数，因为它继承了Component，所以它里面一定要先super()调用父的构造函数
  constructor() {
    super();

    this.state = {
      uname: '小王',
    };
  }

  render() {
    return (
      <div>
        <h1>State状态机</h1>

        {/* 使用定义的数据 */}
        <p>姓名：{this.state.uname}</p>

        <p>
          <button onClick={() => this.changeUname()}>修改uname</button>
        </p>
      </div>
    );
  }

  changeUname() {
    // 语法：this.setState({被修改State数据中的key: 新的value}, callback)
    this.setState({ uname: '老王' }, () => {
      console.log(this.state.uname);
    });
  }
}
