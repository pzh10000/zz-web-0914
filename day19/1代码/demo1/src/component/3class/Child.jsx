import React, { Component } from 'react';

export default class Child extends Component {
  render() {
    // console.log(this); // 当前Child组件的实例，它下面就有props，父组件传过来的数据

    return (
      <div className="well">
        <h2>子组件</h2>
        <p>用传过来的数据</p>
        <p>{this.props.name}</p>
        <p>{this.props.age}</p>
      </div>
    );
  }
}
