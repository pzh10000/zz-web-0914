import React from 'react';

import Child from './Child';

class Index extends React.Component {

  // 类组件里面有两个成员：属性和方法
  // 在方法里面要访问其它的属性或方法，必须加上this

  name = 'zs';
  age = 3;

  render() {
    return (
      <div className="alert alert-info">
        <h1>父组件</h1>
        <p>使用自己的数据</p>
        <p>姓名：{this.name}</p>
        <p>年龄：{this.age}</p>

        <Child name={this.name} age={this.age}></Child>
      </div>
    );
  }
}

export default Index;
