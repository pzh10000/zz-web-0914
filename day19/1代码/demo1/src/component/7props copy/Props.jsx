import React, { Component } from 'react';

import Child from './Child';

export default class Props extends Component {
  render() {
    return (
      <div>
        <h1>父组件</h1>

        <hr />

        <Child abc={(n) => this.abc(n)}></Child>
      </div>
    );
  }

  abc(str) {
    console.log(str);
  }
}
