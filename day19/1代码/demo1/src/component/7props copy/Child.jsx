import React, { Component } from 'react';

export default class Child extends Component {
  render() {
    // console.log(this.props);
    let { abc } = this.props; // abc父组件传过来的箭头函数
    return (
      <div>
        <h1>子组件</h1>

        <p>
          <button onClick={() => abc('我是小王')}>
            调用父组件传过来的方法
          </button>
        </p>
      </div>
    );
  }
}
