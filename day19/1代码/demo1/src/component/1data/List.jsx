import React from 'react';

let arr = ['红烧肉', '油焖大虾', '红烧排骨'];
let list = [
  { id: 1, name: '冰墩墩', price: 58 },
  { id: 2, name: '雪容融', price: 38 },
  { id: 3, name: '防晒霜', price: 90 },
  { id: 4, name: '风扇', price: 300 },
];

export default function List() {
  return (
    <div>
      <h1>列表渲染</h1>

      <h3>中午吃啥</h3>
      <ul>
        {arr.map((item, index) => (
          <li key={index}>{item}</li>
        ))}
      </ul>

      <h3>商品列表</h3>
      <ul>
        {list.map((item) => (
          <li key={item.id}>
            商品名称：{item.name}
            <br />
            价格：{item.price}
          </li>
        ))}
      </ul>
    </div>
  );
}
