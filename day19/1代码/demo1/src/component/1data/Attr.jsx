import React from 'react';

let href = 'https://baidu.com';
let urlImg =
  'https://tupian.qqw21.com/article/UploadPic/2019-9/2019918223638488.jpg';

export default function Attr() {
  return (
    <div>
      <h1>属性绑定</h1>

      <p>
        <a href={href} abc={'你好小芳'}>
          百度
        </a>
      </p>

      <img src={urlImg} alt="" />
    </div>
  );
}
