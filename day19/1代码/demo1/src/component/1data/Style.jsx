import React from 'react';

import './ab.css'; // 引进样式

let style = {
  width: '120px',
  height: '80px',
  background: 'green',
};

export default function Style() {
  return (
    <div>
      <h1>style控制样式</h1>

      <div style={{ width: '100px', height: '100px', background: 'red' }}></div>
      <div style={style}></div>

      <hr />
      <h1>类名的使用</h1>
      <div className="abc">我就是我</div>
    </div>
  );
}
