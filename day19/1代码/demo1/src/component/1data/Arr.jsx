import React from 'react';

let girlFriends = ['小丽', '翠翠', '兰兰'];
let person = {
  name: '小刘',
  nickname: '木马牛',
  age: 18,
};
let brand = [
  { id: 1, name: '皇冠' },
  { id: 2, name: '凯迪拉克' },
  { id: 3, name: '特斯拉' },
];

export default function Arr() {
  return (
    <div>
      <h1>数组、对象的输出</h1>

      <p>{JSON.stringify(girlFriends)}</p>

      <p>{JSON.stringify(person)}</p>

      <p>{JSON.stringify(brand)}</p>
    </div>
  );
}
