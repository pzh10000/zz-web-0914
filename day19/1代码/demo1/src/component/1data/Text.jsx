import React from 'react';

let str = `
  <ul>
    <li>吃饭</li>
    <li>睡觉</li>
    <li>打豆豆</li>
  </ul>
`;

export default function Text() {
  return (
    <div>
      <h1>富文本渲染</h1>

      <div dangerouslySetInnerHTML={{ __html: str }}></div>
    </div>
  );
}
