import React from 'react';

export default function Date() {
  let uname = '孙悟空';
  let age = 18;
  let isPerson = false;

  return (
    <div>
      <h1>数据渲染</h1>

      {/* 大括号中写js */}
      <p>姓名：{uname}</p>
      <p>五年以后年龄：{age + 5}</p>

      {isPerson ? (<p>是个人</p>) : (<p>是个猴</p>)}
    </div>
  );
}
