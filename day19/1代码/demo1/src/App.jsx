import React from 'react';

// jsx语法
// import Date from './component/1data/Date';
// import Attr from './component/1data/Attr';
// import Arr from './component/1data/Arr';
// import List from './component/1data/List';
// import Style from './component/1data/Style';
// import Text from './component/1data/Text';

// 函数组件
// import Fun from './component/2fun/Fun';
// 类组件
// import Index from './component/3class/Index'
// 事件绑定
// import Event from './component/4event/Event';
// 获取事件对象
// import GetEvent from './component/4event/GetEvent';
// 阻止默认事件 / 阻止事件传播 / 事件捕获
// import Stop from './component/4event/Stop';

// state状态机
// import State from './component/5state/State';

// props
// import Props from './component/6props/Props';

// 将方法传给子组件（子传父）
import Props from './component/7props/Props'

export default function App() {
  // 在这里写js
  return (
    <div>
      {/* 在根标签的里面写jsx */}
      <h1>根组件</h1>

      <hr />

      {/* 组件名首字母必须大写，组件名当标签使用 */}
      {/* <Date></Date> */}
      {/* <Attr></Attr> */}
      {/* <Arr></Arr> */}
      {/* <List></List> */}
      {/* <Style></Style> */}
      {/* <Text></Text> */}
      {/* <Fun></Fun> */}
      {/* <Index></Index> */}
      {/* <Event></Event> */}
      {/* <GetEvent></GetEvent> */}
      {/* <Stop></Stop> */}
      {/* <State></State> */}
      <Props></Props>
    </div>
  );
}
