// 1.按需引入路由模块和路由模式
import {
  createRouter,
  createWebHashHistory, // hash
  createWebHistory, // history
} from 'vue-router';

// 2.创建路由规则
const routes = [
  {
    path: '/index',
    component: () => import('../pages/index.vue'),
    children: [
      {
        path: '/home',
        component: () => import('../views/home.vue'),
      },
      {
        path: '/sort',
        component: () => import('../views/sort.vue'),
      },
      {
        path: '/cart',
        component: () => import('../views/cart.vue'),
      },
      {
        path: '/user',
        component: () => import('../views/user.vue'),
      },
      {
        path: '',
        redirect: '/home',
      },
    ],
  },
  {
    path: '/order',
    component: () => import('../pages/order.vue'),
  },
  {
    path: '/search',
    component: () => import('../pages/search.vue'),
  },
  {
    path: '/detail',
    component: () => import('../pages/detail.vue'),
  },
  {
    path: '/list',
    component: () => import('../pages/list.vue'),
  },
  {
    path: '/:catchAll(.*)',
    redirect: '/index',
  },
];

// 3.根据路由规则创建路由实例
let router = createRouter({
  routes, // 路由
  // history: createWebHashHistory(), // hash
  history: createWebHistory(), // history
});

router.beforeEach((to, from, next) => {
  console.log(to.path);
  console.log(from.path);
  next();
});

// 4.导出路由实例
export default router;
