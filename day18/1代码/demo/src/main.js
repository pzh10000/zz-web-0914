import { createApp } from 'vue';
import App from './App.vue';

let app = createApp(App);

// 引入路由
import router from './router';
app.use(router);

// 引入vuex
import store from './store';
app.use(store);

app.mount('#app');
