export default {
  state() {
    return {
      uname: '小王',
    };
  },
  getters: {
    get_uname(state) {
      return state.uname;
    },
  },
  mutations: {
    mutation_uname(state) {
      state.uname = '小不点';
    },
  },
  actions: {
    action_uname({ commit }) {
      setTimeout(() => {
        commit('mutation_uname');
      }, 1000);
    },
  },
  namespaced: true,
};
