// 1.引入
import { createStore } from 'vuex';

// 引入模块
import user from './modules/user';

// 2.实例化仓库
let store = createStore({
  state() {
    return {};
  },
  getters: {},
  mutations: {},
  actions: {},
  modules: {
    user,
  },
});

// 3.导出仓库实例
export default store;
