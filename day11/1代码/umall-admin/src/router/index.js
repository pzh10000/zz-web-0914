import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [
  {
    path: '/login',
    component: () => import('../pages/login.vue'),
  },
  {
    path: '/index',
    component: () => import('../pages/index.vue'),
    children: [
      {
        path: '/home',
        component: () => import('../views/home.vue'),
      },
      {
        path: '/menu',
        component: () => import('../views/menu/menu.vue'),
        meta: {
          title: '菜单管理',
        },
      },
      {
        path: '/role',
        component: () => import('../views/role/role.vue'),
        meta: {
          title: '角色管理',
        },
      },
      {
        path: '/user',
        component: () => import('../views/user/user.vue'),
        meta: {
          title: '管理员管理',
        },
      },
      {
        path: '/cate',
        component: () => import('../views/cate.vue'),
      },
      {
        path: '',
        redirect: '/home',
      },
    ],
  },
  {
    path: '*',
    redirect: '/index',
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
