import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

// 引入模块
import menu from './modules/menu';
import role from './modules/role';
import user from './modules/user'

export default new Vuex.Store({
  state: {},
  getters: {},
  mutations: {},
  actions: {},
  modules: {
    menu,
    role,
    user,
  },
});
