import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    name: '王二小',
    age: 12,
    user: {
      userInfo: {
        name: 'zs',
      },
    },
  },
  getters: {
    get_name(state) {
      return state.name + '快跑';
    },
    get_age(state) {
      return state.age;
    },
    get_user(state) {
      return state.user.userInfo.name;
    },
  },
  mutations: {
    mutation_name(state, payload) {
      // console.log(state); // 唯一的数据源
      // console.log(payload); // 传过来的参数
      state.name = payload;
    },

    mutation_age(state, payload) {
      state.age += payload;
    },
  },
  actions: {
    action_age(context, payload) {
      // console.log(context); // 仓库对象
      // console.log(payload); // 传过来的参数
      setTimeout(() => {
        context.commit('mutation_age', payload);
      }, 2000);
    },
  },
  modules: {},
});
