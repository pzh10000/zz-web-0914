import axios from 'axios';

let http = axios.create({
  baseURL: '/api',
});

// 请求拦截器
http.interceptors.request.use((req) => {
  return req;
});

// 响应拦截器
http.interceptors.response.use((res) => {
  return res.data; // 过滤数据
});

export default http;
