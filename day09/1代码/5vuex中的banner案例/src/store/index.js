import Vue from 'vue';
import Vuex from 'vuex';

import { getbanner } from '../request/api';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    list: [], // banner数据
  },
  getters: {
    get_list(state) {
      return state.list;
    },
  },
  mutations: {
    mutation_list(state, payload) {
      state.list = payload;
    },
  },
  actions: {
    action_list({ commit }) {
      // console.log('我执行了');

      // 发起ajax请求
      getbanner()
        .then((res) => {
          console.log(res);
          if (res.code == 200) {
            commit('mutation_list', res.list);
          }
        })
        .catch((err) => {
          console.log(err);
        });
    },
  },
  modules: {},
});
