import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [
  {
    path: '/pc',
    component: () => import('../components/pcDemo.vue'),
  },
  {
    path: '/vant',
    component: () => import('../components/vantDemo.vue'),
  },
  {
    path: '*',
    redirect: '/pc',
  },
];

const router = new VueRouter({
  routes,
});

export default router;
