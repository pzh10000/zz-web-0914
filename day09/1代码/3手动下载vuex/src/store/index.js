import Vue from 'vue'; // 引入核心库
import Vuex from 'vuex'; // 引入状态管理
Vue.use(Vuex); // 调用vuex插件

// console.log(Vuex); // {} 对象，它下面有Store方法，它是一个构造函数
// console.log(new Vuex.Store()); // 实例，仓库的实例

export default new Vuex.Store({
  // 配置对象
  state: {
    name: '小王',
    age: 18,
  },
  getters: {},
  mutations: {},
  actions: {},
  modules: {},
});
