import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [
  {
    path: '/one',
    component: () => import('../components/one'),
  },
  {
    path: '*',
    redirect: '/one',
  },
];

const router = new VueRouter({
  routes,
});

export default router;
