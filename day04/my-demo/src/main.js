import Vue from 'vue'; // 引入vue核心库
import App from './App.vue'; // 引入根组件

// 开发环境取消一些提示
Vue.config.productionTip = false;

// 全局引入css和js
import './assets/js/rem';
import './assets/css/base.css';

new Vue({
  render: (h) => h(App),
}).$mount('#app');
